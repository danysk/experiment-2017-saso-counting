#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T12:23+0000 #
#####################################################################
# random = 4.0, targets = 1.0, density = 0.11269076455391362, gridShift = 1.0, perturbation = 0.5, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.007462068749906 NaN 
2.007462068749906 NaN 
3.007462068749906 1.0402777777777779 
4.007462068749906 1.6030555555555557 
5.007462068749906 2.1536111111111107 
6.007462068749906 2.879722222222222 
7.007462068749906 5.856111111111111 
8.007462068749906 4.561388888888889 
9.007462068749906 1.9097222222222219 
10.007462068749906 1.0291666666666661 
11.007462068749906 0.22666666666666646 
12.007462068749906 0.1244444444444443 
13.007462068749906 0.01999999999999998 
14.007462068749906 0.009999999999999995 
15.007462068749906 4.930380657631324E-32 
16.007462068749906 4.930380657631324E-32 
17.007462068749906 4.930380657631324E-32 
18.007462068749906 4.930380657631324E-32 
19.007462068749906 4.930380657631324E-32 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T12:23+0000 #
#####################################################################
