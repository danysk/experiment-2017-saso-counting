#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T00:20+0000 #
#####################################################################
# random = 13.0, targets = 10.0, density = 0.07978946395100113, gridShift = 0.0, perturbation = 0.0, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0170171764237625 1.0 
2.0170171764237628 0.988125 
3.0170171764237628 0.988125 
4.017017176423763 0.918125 
5.017017176423763 0.90625 
6.017017176423763 0.88375 
7.017017176423763 0.895625 
8.017017176423764 0.8931250000000001 
9.017017176423764 0.848125 
10.017017176423764 0.821875 
11.017017176423764 0.81 
12.017017176423764 0.81 
13.017017176423764 0.81 
14.017017176423764 0.81 
15.017017176423764 0.81 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T00:20+0000 #
#####################################################################
