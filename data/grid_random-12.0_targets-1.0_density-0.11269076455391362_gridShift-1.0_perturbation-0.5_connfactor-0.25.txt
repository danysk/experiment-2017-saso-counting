#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T13:42+0000 #
#####################################################################
# random = 12.0, targets = 1.0, density = 0.11269076455391362, gridShift = 1.0, perturbation = 0.5, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0242474491697862 NaN 
2.024247449169786 1.0 
3.024247449169786 1.0 
4.024247449169787 0.96 
5.024247449169787 0.8 
6.024247449169787 0.8 
7.024247449169787 0.72 
8.024247449169787 0.88 
9.024247449169787 0.72 
10.024247449169787 0.56 
11.024247449169787 0.28 
12.024247449169787 0.12 
13.024247449169787 0.0 
14.024247449169787 0.0 
15.024247449169787 0.0 
16.024247449169785 0.0 
17.024247449169785 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T13:42+0000 #
#####################################################################
