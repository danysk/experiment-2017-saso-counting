#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T13:25+0000 #
#####################################################################
# random = 15.0, targets = 1.0, density = 0.07978946395100113, gridShift = 1.0, perturbation = 0.5, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.033500917323147 NaN 
2.0335009173231473 1.0 
3.0335009173231473 1.0 
4.033500917323147 1.0 
5.033500917323147 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T13:25+0000 #
#####################################################################
