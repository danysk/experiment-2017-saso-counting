#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T09:48+0000 #
#####################################################################
# random = 14.0, targets = 31.622776601683793, density = 5.0264212595847235, gridShift = 0.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0015837731381736 NaN 
2.0015837731381736 1.0 
3.0015837731381736 1.0 
4.001583773138174 0.9997294029808882 
5.001583773138174 0.9989703424384861 
6.001583773138174 0.9974556935759042 
7.001583773138174 0.9949658537778531 
8.001583773138174 0.9935048034857603 
9.001583773138174 0.9909631012283312 
10.001583773138174 0.9884758655969468 
11.001583773138174 0.9869145626083203 
12.001583773138174 0.9845562255068028 
13.001583773138174 0.9820205996382626 
14.001583773138174 0.9787981827123869 
15.001583773138174 0.9759029918504235 
16.001583773138172 0.9770083756789602 
17.001583773138172 0.9746743441329981 
18.001583773138172 0.9744590817954865 
19.001583773138172 0.9746880121479317 
20.001583773138172 0.9748833092076655 
21.001583773138172 0.9752739033271334 
22.001583773138172 0.9743013165583307 
23.001583773138172 0.9738482224388629 
24.001583773138172 0.9727884038068599 
25.001583773138172 0.9711674258588556 
26.001583773138172 0.9698498101671188 
27.001583773138172 0.9687899915351159 
28.001583773138172 0.9679046366295135 
29.001583773138172 0.966251761499909 
30.001583773138172 0.9660675282884419 
31.001583773138172 0.9649972929897723 
32.00158377313817 0.9643091921289705 
33.00158377313817 0.9640247062209697 
34.00158377313817 0.9619786328052308 
35.00158377313817 0.9609923780214946 
36.00158377313817 0.9616527011045187 
37.00158377313817 0.9604555242610487 
38.00158377313817 0.9586989242126884 
39.00158377313817 0.9573244586021957 
40.00158377313817 0.9559931748955254 
41.00158377313817 0.9538314446572081 
42.00158377313817 0.949025376172217 
43.00158377313817 0.9474864217540796 
44.00158377313817 0.9428350725916667 
45.00158377313817 0.9399435748258809 
46.00158377313817 0.9381322872775205 
47.00158377313817 0.9339683127088868 
48.00158377313817 0.9323265014275491 
49.00158377313817 0.9272773928272685 
50.00158377313817 0.9242112104611266 
51.00158377313817 0.9197523333178256 
52.00158377313817 0.9145762831727445 
53.00158377313817 0.9116758839774478 
54.00158377313817 0.9064360394691664 
55.00158377313817 0.9001908429549268 
56.00158377313817 0.8942077837781988 
57.00158377313817 0.888078038652848 
58.00158377313817 0.8796767619815352 
59.00158377313817 0.8675962610182051 
60.00158377313817 0.8607054271838608 
61.00158377313817 0.8457982868867295 
62.00158377313817 0.8299339409237607 
63.00158377313817 0.8284020892814549 
64.00158377313818 0.8096493508626237 
65.00158377313818 0.794468728582341 
66.00158377313818 0.7922000469377276 
67.00158377313818 0.7699150897108593 
68.00158377313818 0.7730294589715468 
69.00158377313818 0.7583954730113244 
70.00158377313818 0.7501666949704606 
71.00158377313818 0.7489676726244415 
72.00158377313818 0.7249346038202658 
73.00158377313818 0.7280842336214091 
74.00158377313818 0.7107050820274792 
75.00158377313818 0.7051315684287369 
76.00158377313818 0.6960915237715131 
77.00158377313818 0.6774329111573695 
78.00158377313818 0.6647929406683507 
79.00158377313818 0.6596077729300336 
80.00158377313818 0.6428244369311041 
81.00158377313818 0.6368444317798387 
82.00158377313818 0.6288200644632187 
83.00158377313818 0.6293228800833806 
84.00158377313818 0.6345209922352045 
85.00158377313818 0.6187092595382785 
86.00158377313818 0.6199096212587208 
87.00158377313818 0.6138274030252003 
88.00158377313818 0.6115560923531939 
89.00158377313818 0.6178015346021154 
90.00158377313818 0.6099161192450242 
91.00158377313818 0.6132131010173114 
92.00158377313818 0.6091383770580089 
93.00158377313818 0.6082557699916982 
94.00158377313818 0.607424660838185 
95.00158377313818 0.606807185992935 
96.00158377313818 0.608271795129041 
97.00158377313818 0.6099235195680702 
98.00158377313818 0.6070276922545425 
99.00158377313818 0.6090795641343808 
100.00158377313818 0.6079460118111842 
101.00158377313818 0.6075886864378472 
102.00158377313818 0.6081561121361608 
103.00158377313818 0.6079099489049634 
104.00158377313818 0.607258178145136 
105.00158377313818 0.6088089100644349 
106.00158377313818 0.606938641059095 
107.00158377313818 0.6066637037622055 
108.00158377313818 0.605540532514913 
109.00158377313818 0.6054396326367792 
110.00158377313818 0.6055860977114467 
111.00158377313818 0.606117080516693 
112.00158377313818 0.6062811275764269 
113.00158377313818 0.6062811275764269 
114.00158377313818 0.6062811275764269 
115.00158377313818 0.6062811275764269 
116.00158377313818 0.6062811275764269 
117.00158377313818 0.6062811275764269 
118.00158377313818 0.6062811275764269 
119.00158377313818 0.6062811275764269 
120.00158377313818 0.6062811275764269 
121.00158377313818 0.6062811275764269 
122.00158377313818 0.6062811275764269 
123.00158377313818 0.6062811275764269 
124.00158377313818 0.6062811275764269 
125.00158377313818 0.6062811275764269 
126.00158377313818 0.6062811275764269 
127.00158377313818 0.6062811275764269 
128.00158377313818 0.6062811275764269 
129.00158377313818 0.6062811275764269 
130.00158377313818 0.6062811275764269 
131.00158377313818 0.6062811275764269 
132.00158377313818 0.6062811275764269 
133.00158377313818 0.6062811275764269 
134.00158377313818 0.6062811275764269 
135.00158377313818 0.6062811275764269 
136.00158377313818 0.6062811275764269 
137.00158377313818 0.6062811275764269 
138.00158377313818 0.6062811275764269 
139.00158377313818 0.6062811275764269 
140.00158377313818 0.6062811275764269 
141.00158377313818 0.6062811275764269 
142.00158377313818 0.6062811275764269 
143.00158377313818 0.6062811275764269 
144.00158377313818 0.6062811275764269 
145.00158377313818 0.6062811275764269 
146.00158377313818 0.6062811275764269 
147.00158377313818 0.6062811275764269 
148.00158377313818 0.6062811275764269 
149.00158377313818 0.6062811275764269 
150.00158377313818 0.6062811275764269 
151.00158377313818 0.6062811275764269 
152.00158377313818 0.6062811275764269 
153.00158377313818 0.6062811275764269 
154.00158377313818 0.6062811275764269 
155.00158377313818 0.6062811275764269 
156.00158377313818 0.6062811275764269 
157.00158377313818 0.6062811275764269 
158.00158377313818 0.6062811275764269 
159.00158377313818 0.6062811275764269 
160.00158377313818 0.6062811275764269 
161.00158377313818 0.6062811275764269 
162.00158377313818 0.6062811275764269 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T09:48+0000 #
#####################################################################
