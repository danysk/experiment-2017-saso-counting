#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T12:23+0000 #
#####################################################################
# random = 4.0, targets = 1.0, density = 0.1591589639397027, gridShift = 0.0, perturbation = 0.5, connfactor = 1.0
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.007462068749906 NaN 
2.007462068749906 1.0 
3.007462068749906 1.0 
4.007462068749906 1.12 
5.007462068749906 1.4499999999999997 
6.007462068749906 1.2 
7.007462068749906 1.31 
8.007462068749906 1.4899999999999998 
9.007462068749906 0.4499999999999999 
10.007462068749906 0.1799999999999999 
11.007462068749906 0.009999999999999997 
12.007462068749906 0.0 
13.007462068749906 0.0 
14.007462068749906 0.0 
15.007462068749906 0.0 
16.007462068749906 0.0 
17.007462068749906 0.0 
18.007462068749906 0.0 
19.007462068749906 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T12:23+0000 #
#####################################################################
