#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T07:42+0000 #
#####################################################################
# random = 5.0, targets = 3.1622776601683795, density = 0.07978946395100113, gridShift = 0.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.123893470128217 NaN 
2.123893470128217 1.0 
3.123893470128217 1.0 
4.123893470128217 1.0 
5.123893470128217 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T07:42+0000 #
#####################################################################
