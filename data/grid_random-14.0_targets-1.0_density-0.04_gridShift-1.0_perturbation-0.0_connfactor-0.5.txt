#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T05:35+0000 #
#####################################################################
# random = 14.0, targets = 1.0, density = 0.04, gridShift = 1.0, perturbation = 0.0, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.2024264217210618 NaN 
2.2024264217210616 1.0 
3.2024264217210616 0.9166666666666666 
4.202426421721062 0.5 
5.202426421721062 0.25 
6.202426421721062 0.0 
7.202426421721062 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T05:35+0000 #
#####################################################################
