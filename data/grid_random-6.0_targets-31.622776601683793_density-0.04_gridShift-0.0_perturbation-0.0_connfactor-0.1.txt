#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T06:03+0000 #
#####################################################################
# random = 6.0, targets = 31.622776601683793, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0115592716150654 NaN 
2.0115592716150656 1.0 
3.0115592716150656 1.0 
4.011559271615066 1.0 
5.011559271615066 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T06:03+0000 #
#####################################################################
