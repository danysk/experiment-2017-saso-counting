#####################################################################
# Alchemist log file - simulation started at: 2017-05-19T20:15+0000 #
#####################################################################
# random = 0.0, targets = 3.1622776601683795, density = 3.5589026259761294, gridShift = 0.0, perturbation = 0.5, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0011337332612453 NaN 
2.0011337332612453 1.0 
3.0011337332612453 1.0 
4.001133733261245 0.9990570185577217 
5.001133733261245 0.9977258797276375 
6.001133733261245 0.9899605374682715 
7.001133733261245 0.9814188804332938 
8.001133733261245 0.9630044275332544 
9.001133733261245 0.9478070763478559 
10.001133733261245 0.9362917389043344 
11.001133733261245 0.9223487719095894 
12.001133733261245 0.9111806615041349 
13.001133733261245 0.8990066942741711 
14.001133733261245 0.8905315749426839 
15.001133733261245 0.8726668304053801 
16.001133733261245 0.869360548164586 
17.001133733261245 0.8455009674774642 
18.001133733261245 0.8367400094087518 
19.001133733261245 0.8263007047352668 
20.001133733261245 0.802442606313689 
21.001133733261245 0.7980420134074714 
22.001133733261245 0.7871399912099992 
23.001133733261245 0.7625794066479081 
24.001133733261245 0.7485984401236008 
25.001133733261245 0.7451693379385224 
26.001133733261245 0.7224318456946018 
27.001133733261245 0.7284040342059125 
28.001133733261245 0.7286693812811229 
29.001133733261245 0.7091957586627133 
30.001133733261245 0.7279018541501968 
31.001133733261245 0.7249040342059124 
32.00113373326124 0.70752031398305 
33.00113373326124 0.7236825483006177 
34.00113373326124 0.7258777001669228 
35.00113373326124 0.7237922012254073 
36.00113373326124 0.7374390884677646 
37.00113373326124 0.7502833509747431 
38.00113373326124 0.7468754767686065 
39.00113373326124 0.7708184052365952 
40.00113373326124 0.7813095983285315 
41.00113373326124 0.7895815289134894 
42.00113373326124 0.816340249136352 
43.00113373326124 0.8281538174929096 
44.00113373326124 0.842583622284004 
45.00113373326124 0.8565594683007299 
46.00113373326124 0.8743445225625819 
47.00113373326124 0.8936142297492239 
48.00113373326124 0.9238071582172123 
49.00113373326124 0.9372457265737699 
50.00113373326124 0.9607303361559589 
51.00113373326124 0.9681908350974743 
52.00113373326124 0.9675592515877268 
53.00113373326124 0.9569035574373057 
54.00113373326124 1.0491667677994014 
55.00113373326124 1.2561536874651074 
56.00113373326124 1.2357136561063835 
57.00113373326124 1.976048712497489 
58.00113373326124 1.9535604454098803 
59.00113373326124 3.0703027167437105 
60.00113373326124 4.052502459881968 
61.00113373326124 3.3706623985635558 
62.00113373326124 7.153730303459595 
63.00113373326124 6.803625010646254 
64.00113373326124 9.752833806300643 
65.00113373326124 10.591729635071808 
66.00113373326124 6.81470490318718 
67.00113373326124 8.701513784462172 
68.00113373326124 7.280565619906319 
69.00113373326124 6.805493143508959 
70.00113373326124 11.39764458400383 
71.00113373326124 7.76633131372233 
72.00113373326124 9.61508887434123 
73.00113373326124 10.28515204348998 
74.00113373326124 8.569032048417291 
75.00113373326124 11.742783285582053 
76.00113373326124 10.892530316235117 
77.00113373326124 10.353217421589505 
78.00113373326124 11.210387202078442 
79.00113373326124 12.68752785255185 
80.00113373326124 12.688288666145226 
81.00113373326124 10.957826487488557 
82.00113373326124 10.61270510165137 
83.00113373326124 9.98833365802404 
84.00113373326124 10.092392207005755 
85.00113373326124 5.164952872929925 
86.00113373326124 4.264008322536907 
87.00113373326124 2.5472683034964634 
88.00113373326124 2.0433347400672 
89.00113373326124 2.538563627668534 
90.00113373326124 0.5364171540815295 
91.00113373326124 1.031092642103791 
92.00113373326124 0.030533303830863223 
93.00113373326124 0.025028914824364937 
94.00113373326124 0.016663395443377305 
95.00113373326124 0.012166321447709589 
96.00113373326124 0.008977709748551416 
97.00113373326124 0.007977709748551463 
98.00113373326124 0.007153154428214708 
99.00113373326124 0.004133403898972396 
100.00113373326124 0.004308848578635663 
101.00113373326124 0.002633403898972407 
102.00113373326124 0.002633403898972407 
103.00113373326124 0.002633403898972407 
104.00113373326124 0.002633403898972407 
105.00113373326124 0.002633403898972407 
106.00113373326124 0.002633403898972407 
107.00113373326124 0.002633403898972407 
108.00113373326124 0.002633403898972407 
109.00113373326124 0.002633403898972407 
110.00113373326124 0.002633403898972407 
111.00113373326124 0.002633403898972407 
112.00113373326124 0.002633403898972407 
113.00113373326124 0.002633403898972407 
114.00113373326124 0.002633403898972407 
115.00113373326124 0.002633403898972407 
116.00113373326124 0.002633403898972407 
117.00113373326124 0.002633403898972407 
118.00113373326124 0.002633403898972407 
119.00113373326124 0.002633403898972407 
120.00113373326124 0.002633403898972407 
121.00113373326124 0.002633403898972407 
122.00113373326124 0.002633403898972407 
123.00113373326124 0.002633403898972407 
124.00113373326124 0.002633403898972407 
125.00113373326124 0.002633403898972407 
126.00113373326124 0.002633403898972407 
127.00113373326124 0.002633403898972407 
128.00113373326124 0.002633403898972407 
129.00113373326124 0.002633403898972407 
130.00113373326124 0.002633403898972407 
131.00113373326124 0.002633403898972407 
132.00113373326124 0.002633403898972407 
133.00113373326124 0.002633403898972407 
134.00113373326124 0.002633403898972407 
135.00113373326124 0.002633403898972407 
136.00113373326124 0.002633403898972407 
137.00113373326124 0.002633403898972407 
138.00113373326124 0.002633403898972407 
139.00113373326124 0.002633403898972407 
140.00113373326124 0.002633403898972407 
141.00113373326124 0.002633403898972407 
142.00113373326124 0.002633403898972407 
143.00113373326124 0.002633403898972407 
144.00113373326124 0.002633403898972407 
#####################################################################
# End of data export. Simulation finished at: 2017-05-19T20:18+0000 #
#####################################################################
