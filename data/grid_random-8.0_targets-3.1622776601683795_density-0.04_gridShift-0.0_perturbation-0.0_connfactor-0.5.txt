#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T10:18+0000 #
#####################################################################
# random = 8.0, targets = 3.1622776601683795, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0490773234798803 NaN 
2.0490773234798803 1.0 
3.0490773234798803 1.0 
4.04907732347988 0.9875182875270762 
5.04907732347988 0.9471697358789627 
6.04907732347988 0.9010143557721652 
7.04907732347988 0.812002206669681 
8.04907732347988 0.7249568982125042 
9.04907732347988 0.6181953290388217 
10.04907732347988 0.5200183661016012 
11.04907732347988 0.48939203667570996 
12.04907732347988 0.4611779752782982 
13.04907732347988 0.4484449899022458 
14.04907732347988 0.4675444679663244 
15.04907732347988 0.4675444679663244 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T10:18+0000 #
#####################################################################
