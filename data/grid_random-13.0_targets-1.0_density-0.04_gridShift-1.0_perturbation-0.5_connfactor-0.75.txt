#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T21:46+0000 #
#####################################################################
# random = 13.0, targets = 1.0, density = 0.04, gridShift = 1.0, perturbation = 0.5, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0170171764237625 NaN 
2.0170171764237628 1.0 
3.0170171764237628 0.9375 
4.017017176423763 0.75 
5.017017176423763 0.625 
6.017017176423763 0.5625 
7.017017176423763 0.3125 
8.017017176423764 0.41666666666666663 
9.017017176423764 0.3125 
10.017017176423764 0.25 
11.017017176423764 0.47916666666666663 
12.017017176423764 0.08333333333333334 
13.017017176423764 0.14583333333333331 
14.017017176423764 0.02083333333333333 
15.017017176423764 0.10416666666666666 
16.017017176423764 0.0 
17.017017176423764 0.0 
18.017017176423764 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T21:46+0000 #
#####################################################################
