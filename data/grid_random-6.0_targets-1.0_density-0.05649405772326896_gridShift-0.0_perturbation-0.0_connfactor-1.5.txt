#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T22:06+0000 #
#####################################################################
# random = 6.0, targets = 1.0, density = 0.05649405772326896, gridShift = 0.0, perturbation = 0.0, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0115592716150654 NaN 
2.0115592716150656 5.390625 
3.0115592716150656 8.69140625 
4.011559271615066 9.49782986111111 
5.011559271615066 7.731770833333333 
6.011559271615066 7.511284722222222 
7.011559271615066 6.622395833333334 
8.011559271615067 2.434461805555556 
9.011559271615067 0.4565972222222221 
10.011559271615067 0.25390625 
11.011559271615067 0.08203125 
12.011559271615067 0.01171875 
13.011559271615067 0.0 
14.011559271615067 0.0 
15.011559271615067 0.0 
16.011559271615067 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T22:06+0000 #
#####################################################################
