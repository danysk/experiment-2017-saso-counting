#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T05:36+0000 #
#####################################################################
# random = 14.0, targets = 1.0, density = 0.2247883923996312, gridShift = 0.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.021284317112984 NaN 
2.021284317112984 1.0 
3.021284317112984 1.0 
4.021284317112984 1.0 
5.021284317112984 1.0 
6.021284317112984 1.0 
7.021284317112984 1.0 
8.021284317112984 1.0 
9.021284317112984 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T05:36+0000 #
#####################################################################
