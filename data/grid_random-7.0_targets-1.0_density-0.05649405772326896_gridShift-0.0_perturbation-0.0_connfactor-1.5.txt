#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T14:55+0000 #
#####################################################################
# random = 7.0, targets = 1.0, density = 0.05649405772326896, gridShift = 0.0, perturbation = 0.0, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0724373403975138 NaN 
2.0724373403975136 1.0 
3.0724373403975136 0.990017361111111 
4.072437340397514 1.07421875 
5.072437340397514 1.0460069444444444 
6.072437340397514 1.4275173611111114 
7.072437340397514 0.9713541666666672 
8.072437340397514 0.109375 
9.072437340397514 0.09375 
10.072437340397514 0.0 
11.072437340397514 0.0 
12.072437340397514 0.0 
13.072437340397514 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T14:55+0000 #
#####################################################################
