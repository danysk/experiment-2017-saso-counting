#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T10:36+0000 #
#####################################################################
# random = 17.0, targets = 100.0, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.002823245690351 NaN 
2.002823245690351 1.13741875 
3.002823245690351 1.1045210069444444 
4.002823245690351 0.9647965277777778 
5.002823245690351 0.6904098958333333 
6.002823245690351 0.41411059027777786 
7.002823245690351 0.2833812500000001 
8.002823245690351 0.19058038194444452 
9.002823245690351 0.14011736111111117 
10.002823245690351 0.01 
11.002823245690351 0.01 
12.002823245690351 0.01 
13.002823245690351 0.01 
14.002823245690351 0.01 
15.002823245690351 0.01 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T10:36+0000 #
#####################################################################
