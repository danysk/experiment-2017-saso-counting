#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T21:12+0000 #
#####################################################################
# random = 16.0, targets = 1.0, density = 0.11269076455391362, gridShift = 1.0, perturbation = 0.0, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0086259768480754 NaN 
2.0086259768480756 1.0 
3.0086259768480756 1.0 
4.008625976848076 0.97 
5.008625976848076 0.88 
6.008625976848076 0.73 
7.008625976848076 0.54 
8.008625976848077 0.48 
9.008625976848077 0.38999999999999996 
10.008625976848077 0.15 
11.008625976848077 0.0 
12.008625976848077 0.0 
13.008625976848077 0.0 
14.008625976848077 0.0 
15.008625976848077 0.0 
16.008625976848077 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T21:12+0000 #
#####################################################################
