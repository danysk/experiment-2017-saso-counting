#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T08:26+0000 #
#####################################################################
# random = 11.0, targets = 10.0, density = 3.5589026259761294, gridShift = 0.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0028758930102715 NaN 
2.0028758930102715 1.0 
3.0028758930102715 1.0 
4.0028758930102715 1.0 
5.0028758930102715 1.0 
6.0028758930102715 1.0 
7.0028758930102715 1.0 
8.002875893010271 1.0 
9.002875893010271 1.0 
10.002875893010271 1.0 
11.002875893010271 1.0 
12.002875893010271 1.0 
13.002875893010271 1.0 
14.002875893010271 1.0 
15.002875893010271 1.0 
16.00287589301027 1.0 
17.00287589301027 1.0 
18.00287589301027 1.0 
19.00287589301027 1.0 
20.00287589301027 1.0 
21.00287589301027 1.0 
22.00287589301027 1.0 
23.00287589301027 1.0 
24.00287589301027 1.0 
25.00287589301027 1.0 
26.00287589301027 0.999525 
27.00287589301027 0.9972 
28.00287589301027 0.993825 
29.00287589301027 0.987425 
30.00287589301027 0.985575 
31.00287589301027 0.9767999999999999 
32.00287589301027 0.9672749999999999 
33.00287589301027 0.96105 
34.00287589301027 0.9404750000000001 
35.00287589301027 0.9389249999999999 
36.00287589301027 0.9225499999999999 
37.00287589301027 0.9191250000000001 
38.00287589301027 0.9016 
39.00287589301027 0.889625 
40.00287589301027 0.8746 
41.00287589301027 0.8641 
42.00287589301027 0.8513499999999999 
43.00287589301027 0.830175 
44.00287589301027 0.8346750000000001 
45.00287589301027 0.832125 
46.00287589301027 0.8305750000000001 
47.00287589301027 0.8179500000000001 
48.00287589301027 0.816375 
49.00287589301027 0.807475 
50.00287589301027 0.814475 
51.00287589301027 0.8105 
52.00287589301027 0.79845 
53.00287589301027 0.797825 
54.00287589301027 0.7752999999999999 
55.00287589301027 0.775775 
56.00287589301027 0.76955 
57.00287589301027 0.7692 
58.00287589301027 0.7728250000000001 
59.00287589301027 0.775175 
60.00287589301027 0.7785000000000001 
61.00287589301027 0.7863 
62.00287589301027 0.787575 
63.00287589301027 0.7742 
64.00287589301027 0.787675 
65.00287589301027 0.790425 
66.00287589301027 0.7885500000000001 
67.00287589301027 0.786675 
68.00287589301027 0.7904000000000001 
69.00287589301027 0.7834000000000001 
70.00287589301027 0.7848 
71.00287589301027 0.796 
72.00287589301027 0.7918000000000001 
73.00287589301027 0.789 
74.00287589301027 0.7904000000000001 
75.00287589301027 0.7792 
76.00287589301027 0.7946000000000001 
77.00287589301027 0.7946000000000001 
78.00287589301027 0.796 
79.00287589301027 0.8072 
80.00287589301027 0.8086000000000001 
81.00287589301027 0.81 
82.00287589301027 0.81 
83.00287589301027 0.81 
84.00287589301027 0.81 
85.00287589301027 0.81 
86.00287589301027 0.81 
87.00287589301027 0.81 
88.00287589301027 0.81 
89.00287589301027 0.81 
90.00287589301027 0.81 
91.00287589301027 0.81 
92.00287589301027 0.81 
93.00287589301027 0.81 
94.00287589301027 0.81 
95.00287589301027 0.81 
96.00287589301027 0.81 
97.00287589301027 0.81 
98.00287589301027 0.81 
99.00287589301027 0.81 
100.00287589301027 0.81 
101.00287589301027 0.81 
102.00287589301027 0.81 
103.00287589301027 0.81 
104.00287589301027 0.81 
105.00287589301027 0.81 
106.00287589301027 0.81 
107.00287589301027 0.81 
108.00287589301027 0.81 
109.00287589301027 0.81 
110.00287589301027 0.81 
111.00287589301027 0.81 
112.00287589301027 0.81 
113.00287589301027 0.81 
114.00287589301027 0.81 
115.00287589301027 0.81 
116.00287589301027 0.81 
117.00287589301027 0.81 
118.00287589301027 0.81 
119.00287589301027 0.81 
120.00287589301027 0.81 
121.00287589301027 0.81 
122.00287589301027 0.81 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T08:27+0000 #
#####################################################################
