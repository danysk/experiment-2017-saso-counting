#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T21:52+0000 #
#####################################################################
# random = 10.0, targets = 1.0, density = 1.2632455339402642, gridShift = 0.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0036005446229952 NaN 
2.003600544622995 1.0 
3.003600544622995 1.0 
4.003600544622995 1.0 
5.003600544622995 1.0 
6.003600544622995 1.0 
7.003600544622995 1.0 
8.003600544622994 1.0 
9.003600544622994 1.0 
10.003600544622994 1.0 
11.003600544622994 1.0 
12.003600544622994 1.0 
13.003600544622994 1.0 
14.003600544622994 1.0 
15.003600544622994 1.0 
16.003600544622994 1.0 
17.003600544622994 1.0 
18.003600544622994 1.0 
19.003600544622994 1.0 
20.003600544622994 1.0 
21.003600544622994 1.0 
22.003600544622994 1.0 
23.003600544622994 1.0 
24.003600544622994 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T21:52+0000 #
#####################################################################
