#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T05:20+0000 #
#####################################################################
# random = 5.0, targets = 1.0, density = 0.1591589639397027, gridShift = 0.0, perturbation = 0.0, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.1058771900388789 NaN 
2.105877190038879 1.0 
3.105877190038879 1.0 
4.105877190038878 1.0 
5.105877190038878 1.0 
6.105877190038878 1.0 
7.105877190038878 1.0 
8.105877190038878 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T05:20+0000 #
#####################################################################
