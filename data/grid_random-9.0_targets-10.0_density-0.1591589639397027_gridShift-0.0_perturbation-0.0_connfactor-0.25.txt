#####################################################################
# Alchemist log file - simulation started at: 2017-05-26T05:32+0000 #
#####################################################################
# random = 9.0, targets = 10.0, density = 0.1591589639397027, gridShift = 0.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0274843760556778 NaN 
2.0274843760556776 1.0 
3.0274843760556776 0.9973777777777778 
4.0274843760556776 0.9819111111111111 
5.0274843760556776 0.9488888888888889 
6.0274843760556776 0.8922666666666667 
7.0274843760556776 0.8069333333333334 
8.027484376055678 0.7252000000000001 
9.027484376055678 0.674 
10.027484376055678 0.46599999999999997 
11.027484376055678 0.4848 
12.027484376055678 0.49 
13.027484376055678 0.49 
14.027484376055678 0.49 
15.027484376055678 0.49 
16.02748437605568 0.49 
17.02748437605568 0.49 
18.02748437605568 0.49 
#####################################################################
# End of data export. Simulation finished at: 2017-05-26T05:32+0000 #
#####################################################################
