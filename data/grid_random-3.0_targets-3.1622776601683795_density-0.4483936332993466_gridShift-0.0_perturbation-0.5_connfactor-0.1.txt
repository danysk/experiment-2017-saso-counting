#####################################################################
# Alchemist log file - simulation started at: 2017-05-21T22:07+0000 #
#####################################################################
# random = 3.0, targets = 3.1622776601683795, density = 0.4483936332993466, gridShift = 0.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0016359619213666 NaN 
2.001635961921367 1.0 
3.001635961921367 1.0 
4.001635961921367 1.0 
5.001635961921367 1.0 
6.001635961921367 1.0 
7.001635961921367 1.0 
8.001635961921366 1.0 
9.001635961921366 1.0 
10.001635961921366 1.0 
11.001635961921366 1.0 
12.001635961921366 1.0 
13.001635961921366 1.0 
14.001635961921366 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-21T22:07+0000 #
#####################################################################
