#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T17:23+0000 #
#####################################################################
# random = 4.0, targets = 10.0, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 2.0
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.007462068749906 NaN 
2.007462068749906 3.1403125 
3.007462068749906 3.399392361111111 
4.007462068749906 4.820243055555555 
5.007462068749906 8.837625868055554 
6.007462068749906 8.506861979166665 
7.007462068749906 8.567903645833333 
8.007462068749906 4.329600694444444 
9.007462068749906 2.0652473958333335 
10.007462068749906 1.7013888888888893 
11.007462068749906 0.3402777777777779 
12.007462068749906 3.1554436208840473E-32 
13.007462068749906 3.1554436208840473E-32 
14.007462068749906 3.1554436208840473E-32 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T17:23+0000 #
#####################################################################
