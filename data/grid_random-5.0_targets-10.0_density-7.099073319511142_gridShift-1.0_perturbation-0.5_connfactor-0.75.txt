#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T11:05+0000 #
#####################################################################
# random = 5.0, targets = 10.0, density = 7.099073319511142, gridShift = 1.0, perturbation = 0.5, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0031365367908396 NaN 
2.00313653679084 1.0 
3.00313653679084 0.9993849715099715 
4.00313653679084 0.9968817663817664 
5.00313653679084 0.9931036324786324 
6.00313653679084 0.9853500712250711 
7.00313653679084 0.9805413105413106 
8.00313653679084 0.9793547008547008 
9.00313653679084 0.9713543447293448 
10.00313653679084 0.9744180911680911 
11.00313653679084 0.9651997863247864 
12.00313653679084 0.9638621794871796 
13.00313653679084 0.9638693019943019 
14.00313653679084 0.9596570512820514 
15.00313653679084 0.9585402421652424 
16.00313653679084 0.9540299145299143 
17.00313653679084 0.9480779914529917 
18.00313653679084 0.9382026353276355 
19.00313653679084 0.9296093304843307 
20.00313653679084 0.9096695156695155 
21.00313653679084 0.8932222222222223 
22.00313653679084 0.8754732905982906 
23.00313653679084 0.8536096866096867 
24.00313653679084 0.8342115384615385 
25.00313653679084 0.8153019943019943 
26.00313653679084 0.8010235042735044 
27.00313653679084 0.7862631766381767 
28.00313653679084 0.7707425213675214 
29.00313653679084 0.7665356125356125 
30.00313653679084 0.7515032051282051 
31.00313653679084 0.7414558404558406 
32.00313653679084 0.7288557692307693 
33.00313653679084 0.734008547008547 
34.00313653679084 0.7082051282051283 
35.00313653679084 0.7058529202279202 
36.00313653679084 0.6967539173789175 
37.00313653679084 0.6880416666666667 
38.00313653679084 0.6838422364672365 
39.00313653679084 0.6865840455840456 
40.00313653679084 0.6893650284900286 
41.00313653679084 0.6842770655270656 
42.00313653679084 0.6870790598290599 
43.00313653679084 0.6710103276353276 
44.00313653679084 0.68228952991453 
45.00313653679084 0.6727795584045583 
46.00313653679084 0.6670284900284901 
47.00313653679084 0.6705929487179487 
48.00313653679084 0.6559807692307693 
49.00313653679084 0.6514907407407408 
50.00313653679084 0.644784544159544 
51.00313653679084 0.6292403846153849 
52.00313653679084 0.628636396011396 
53.00313653679084 0.6200156695156696 
54.00313653679084 0.6002742165242168 
55.00313653679084 0.5963126780626782 
56.00313653679084 0.5883660968660968 
57.00313653679084 0.5707596153846152 
58.00313653679084 0.5624861111111109 
59.00313653679084 0.5571000712250712 
60.00313653679084 0.5417688746438748 
61.00313653679084 0.5465523504273504 
62.00313653679084 0.536471153846154 
63.00313653679084 0.5227856125356128 
64.00313653679083 0.5197792022792026 
65.00313653679083 0.5013853276353276 
66.00313653679083 0.4923878205128203 
67.00313653679083 0.48027386039886044 
68.00313653679083 0.47003988603988617 
69.00313653679083 0.46408511396011415 
70.00313653679083 0.45299394586894576 
71.00313653679083 0.4461787749287746 
72.00313653679083 0.4347012108262107 
73.00313653679083 0.42458725071225095 
74.00313653679083 0.4205779914529916 
75.00313653679083 0.4547549857549858 
76.00313653679083 0.4128970797720798 
77.00313653679083 0.5718596866096862 
78.00313653679083 0.502878917378917 
79.00313653679083 0.7567467948717952 
80.00313653679083 0.9463016381766399 
81.00313653679083 0.8157325498575502 
82.00313653679083 0.9996257122507135 
83.00313653679083 1.0853554131054137 
84.00313653679083 1.0843329772079766 
85.00313653679083 1.171087606837608 
86.00313653679083 1.396889601139603 
87.00313653679083 1.1285363247863247 
88.00313653679083 1.1103447293447286 
89.00313653679083 1.3017660256410268 
90.00313653679083 1.1825331196581188 
91.00313653679083 1.3276695156695155 
92.00313653679083 1.3596627492877493 
93.00313653679083 1.2448240740740748 
94.00313653679083 1.3237738603988607 
95.00313653679083 1.0487549857549863 
96.00313653679083 1.0461666666666667 
97.00313653679083 1.2914038461538475 
98.00313653679083 1.050242521367521 
99.00313653679083 1.3659490740740743 
100.00313653679083 1.1866826923076923 
101.00313653679083 1.8600423789173786 
102.00313653679083 2.046274928774928 
103.00313653679083 1.7368835470085469 
104.00313653679083 2.211213675213675 
105.00313653679083 2.0993767806267805 
106.00313653679083 2.3145195868945883 
107.00313653679083 2.4999725783475784 
108.00313653679083 2.6088247863247864 
109.00313653679083 2.093888176638177 
110.00313653679083 1.957777421652422 
111.00313653679083 2.1705160256410267 
112.00313653679083 1.9099252136752136 
113.00313653679083 2.192931623931622 
114.00313653679083 2.2862977207977218 
115.00313653679083 1.9928265669515663 
116.00313653679083 2.178940527065528 
117.00313653679083 1.665005341880342 
118.00313653679083 1.664087962962964 
119.00313653679083 2.0447702991452963 
120.00313653679083 1.7212389601139575 
121.00313653679083 1.7189202279202305 
122.00313653679083 1.6574697293447296 
123.00313653679083 1.9731876780626738 
124.00313653679083 1.5882898860398853 
125.00313653679083 1.5250587606837624 
126.00313653679083 1.7147499999999887 
127.00313653679083 1.140539886039873 
128.00313653679083 1.5879512108261968 
129.00313653679083 1.5852592592592731 
130.00313653679083 0.9435049857549757 
131.00313653679083 0.9434472934473037 
132.00313653679083 0.6860381054131033 
133.00313653679083 0.3633660968660942 
134.00313653679083 0.235216524216523 
135.00313653679083 0.17098896011395992 
136.00313653679083 0.23284081196581163 
137.00313653679083 0.040548076923076916 
138.00313653679083 0.04030128205128205 
139.00313653679083 0.04062499999999999 
140.00313653679083 0.04 
141.00313653679083 0.04 
142.00313653679083 0.04 
143.00313653679083 0.04 
144.00313653679083 0.04 
145.00313653679083 0.04 
146.00313653679083 0.04 
147.00313653679083 0.04 
148.00313653679083 0.04 
149.00313653679083 0.04 
150.00313653679083 0.04 
151.00313653679083 0.04 
152.00313653679083 0.04 
153.00313653679083 0.04 
154.00313653679083 0.04 
155.00313653679083 0.04 
156.00313653679083 0.04 
157.00313653679083 0.04 
158.00313653679083 0.04 
159.00313653679083 0.04 
160.00313653679083 0.04 
161.00313653679083 0.04 
162.00313653679083 0.04 
163.00313653679083 0.04 
164.00313653679083 0.04 
165.00313653679083 0.04 
166.00313653679083 0.04 
167.00313653679083 0.04 
168.00313653679083 0.04 
169.00313653679083 0.04 
170.00313653679083 0.04 
171.00313653679083 0.04 
172.00313653679083 0.04 
173.00313653679083 0.04 
174.00313653679083 0.04 
175.00313653679083 0.04 
176.00313653679083 0.04 
177.00313653679083 0.04 
178.00313653679083 0.04 
179.00313653679083 0.04 
180.00313653679083 0.04 
181.00313653679083 0.04 
182.00313653679083 0.04 
183.00313653679083 0.04 
184.00313653679083 0.04 
185.00313653679083 0.04 
186.00313653679083 0.04 
187.00313653679083 0.04 
188.00313653679083 0.04 
189.00313653679083 0.04 
190.00313653679083 0.04 
191.00313653679083 0.04 
192.00313653679083 0.04 
193.00313653679083 0.04 
194.00313653679083 0.04 
195.00313653679083 0.04 
196.00313653679083 0.04 
197.00313653679083 0.04 
198.00313653679083 0.04 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T11:09+0000 #
#####################################################################
