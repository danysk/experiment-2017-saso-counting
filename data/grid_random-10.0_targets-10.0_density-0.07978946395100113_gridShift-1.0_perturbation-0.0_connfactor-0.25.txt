#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T00:29+0000 #
#####################################################################
# random = 10.0, targets = 10.0, density = 0.07978946395100113, gridShift = 1.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0042217987811537 NaN 
2.0042217987811535 1.0 
3.0042217987811535 0.995125 
4.0042217987811535 0.976375 
5.0042217987811535 0.956875 
6.0042217987811535 0.957625 
7.0042217987811535 0.929125 
8.004221798781153 0.881125 
9.004221798781153 0.814375 
10.004221798781153 0.7435 
11.004221798781153 0.728125 
12.004221798781153 0.65875 
13.004221798781153 0.63625 
14.004221798781153 0.64 
15.004221798781153 0.64 
16.004221798781153 0.64 
17.004221798781153 0.64 
18.004221798781153 0.64 
19.004221798781153 0.64 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T00:29+0000 #
#####################################################################
