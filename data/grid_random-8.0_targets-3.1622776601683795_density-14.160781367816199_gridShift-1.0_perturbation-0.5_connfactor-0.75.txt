#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T11:39+0000 #
#####################################################################
# random = 8.0, targets = 3.1622776601683795, density = 14.160781367816199, gridShift = 1.0, perturbation = 0.5, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0009270308328329 NaN 
2.000927030832833 1.0011283986971455 
3.000927030832833 1.0022567973942913 
4.000927030832833 1.0024275560140774 
5.000927030832833 1.0016573057074412 
6.000927030832833 0.9979369746748994 
7.000927030832833 0.9943162370291738 
8.000927030832834 0.9921866871003256 
9.000927030832834 0.9921207430529932 
10.000927030832834 0.9909211791228778 
11.000927030832834 0.9914823808410219 
12.000927030832834 0.9913392762998621 
13.000927030832834 0.9930284896775416 
14.000927030832834 0.9937327959368455 
15.000927030832834 0.9940133967959176 
16.000927030832834 0.9968306218331329 
17.000927030832834 0.9971827749627847 
18.000927030832834 0.9975349280924367 
19.000927030832834 0.9978870812220886 
20.000927030832834 0.9985913874813924 
21.000927030832834 0.9992956937406962 
22.000927030832834 1.0 
23.000927030832834 1.0 
24.000927030832834 1.0 
25.000927030832834 1.0 
26.000927030832834 1.0 
27.000927030832834 1.0 
28.000927030832834 1.0 
29.000927030832834 1.0 
30.000927030832834 0.9998073890436396 
31.000927030832834 0.9996147780872793 
32.000927030832834 0.9990369452181983 
33.000927030832834 0.997688668523676 
34.000927030832834 0.9963403918291536 
35.000927030832834 0.99441428226555 
36.000927030832834 0.9930660055710275 
37.000927030832834 0.9903694521819827 
38.000927030832834 0.9890211754874606 
39.000927030832834 0.9857467892293345 
40.000927030832834 0.9845911234911724 
41.000927030832834 0.9822797920148485 
42.000927030832834 0.9805462934076054 
43.000927030832834 0.9782349619312807 
44.000927030832834 0.9749605756731556 
45.000927030832834 0.97207141132775 
46.000927030832834 0.9691822469823448 
47.000927030832834 0.9662930826369394 
48.000927030832834 0.9622482525533724 
49.000927030832834 0.9603163463438487 
50.000927030832834 0.960882585921089 
51.000927030832834 0.9631707308137304 
52.000927030832834 0.9600947521578852 
53.000927030832834 0.9644958209245706 
54.000927030832834 0.9685505332075238 
55.000927030832834 0.9647644516464549 
56.000927030832834 0.9835680359709235 
57.000927030832834 1.0157060729801153 
58.000927030832834 1.0884669163738723 
59.000927030832834 1.0614703952679945 
60.000927030832834 1.1596926840510615 
61.000927030832834 1.2222758614643048 
62.000927030832834 1.2196727126824216 
63.000927030832834 1.2679495240812286 
64.00092703083283 1.2311158968326383 
65.00092703083283 1.2838356992329854 
66.00092703083283 1.203360550673825 
67.00092703083283 1.3601153697561255 
68.00092703083283 1.2729394499187159 
69.00092703083283 1.2760137072898399 
70.00092703083283 1.4767482738285822 
71.00092703083283 1.320657194676401 
72.00092703083283 1.359533910991062 
73.00092703083283 1.2878916554409061 
74.00092703083283 1.3173331441571923 
75.00092703083283 1.2275540500572308 
76.00092703083283 1.3273874394185259 
77.00092703083283 1.2244266501826606 
78.00092703083283 1.348623399890737 
79.00092703083283 1.2298327520234482 
80.00092703083283 1.3446924772153912 
81.00092703083283 1.3491099836142115 
82.00092703083283 1.1823724737765051 
83.00092703083283 1.4439460985195562 
84.00092703083283 1.2304529667848632 
85.00092703083283 1.2718056200888999 
86.00092703083283 1.3068724711399708 
87.00092703083283 1.2147499749514368 
88.00092703083283 1.2787864626311196 
89.00092703083283 1.1574744467204625 
90.00092703083283 1.2228766367053543 
91.00092703083283 1.0311969303848392 
92.00092703083283 1.1427410905822393 
93.00092703083283 0.9496661321848622 
94.00092703083283 0.9566600817704749 
95.00092703083283 1.0865013661894007 
96.00092703083283 0.8625652696150845 
97.00092703083283 1.0493974175110057 
98.00092703083283 0.8916472846119776 
99.00092703083283 0.832164648728457 
100.00092703083283 0.838834897544889 
101.00092703083283 0.7596964425806689 
102.00092703083283 0.7847560349063244 
103.00092703083283 0.6711368792466372 
104.00092703083283 0.6782741510988393 
105.00092703083283 0.6317343827993448 
106.00092703083283 0.6246170624945558 
107.00092703083283 0.6122267303255337 
108.00092703083283 0.5691092869733526 
109.00092703083283 0.5912228624680053 
110.00092703083283 0.5707310501963475 
111.00092703083283 0.5508406724437837 
112.00092703083283 0.648195819152473 
113.00092703083283 0.6669353997137916 
114.00092703083283 0.6892257405553878 
115.00092703083283 0.7604952103576613 
116.00092703083283 0.8447539616803832 
117.00092703083283 0.8640928556981408 
118.00092703083283 0.8883574540648072 
119.00092703083283 0.8757081887727108 
120.00092703083283 0.9095348777136594 
121.00092703083283 0.8663312332672332 
122.00092703083283 0.9941421358855248 
123.00092703083283 0.9484109422602673 
124.00092703083283 0.9524577572191691 
125.00092703083283 1.1422931512770347 
126.00092703083283 1.020782400606032 
127.00092703083283 1.0214114879297225 
128.00092703083283 0.9516762014179346 
129.00092703083283 0.9466688971089801 
130.00092703083283 0.8948700745058429 
131.00092703083283 0.9393321329836438 
132.00092703083283 0.9021614942114218 
133.00092703083283 0.9814950898908251 
134.00092703083283 0.9115370366139482 
135.00092703083283 1.0055843157631759 
136.00092703083283 1.0225681608911343 
137.00092703083283 0.8914778953091974 
138.00092703083283 1.0919562137815038 
139.00092703083283 0.9456746045298652 
140.00092703083283 0.9374861301472656 
141.00092703083283 0.9845384226083378 
142.00092703083283 0.9547196755531024 
143.00092703083283 1.0116525700228247 
144.00092703083283 0.9592376118415677 
145.00092703083283 1.059581083379686 
146.00092703083283 0.9440774578243325 
147.00092703083283 1.0188713311284112 
148.00092703083283 1.0137453260322578 
149.00092703083283 1.030715643954356 
150.00092703083283 1.1252788681760417 
151.00092703083283 0.9994043736611026 
152.00092703083283 1.0320098271138394 
153.00092703083283 1.0895706235877969 
154.00092703083283 0.9690501819992908 
155.00092703083283 1.0209173371251317 
156.00092703083283 0.996660338227214 
157.00092703083283 1.0052162690134592 
158.00092703083283 0.9966348459802967 
159.00092703083283 0.8909562328492635 
160.00092703083283 0.785325646780775 
161.00092703083283 0.9729241300160828 
162.00092703083283 0.8263590183973147 
163.00092703083283 0.861690414616809 
164.00092703083283 0.856618896463474 
165.00092703083283 0.7264547095572079 
166.00092703083283 0.7390135061490903 
167.00092703083283 0.5970549648453716 
168.00092703083283 0.4925674288457554 
169.00092703083283 0.4654715231602613 
170.00092703083283 0.39554860239762524 
171.00092703083283 0.25282524205414525 
172.00092703083283 0.29179670359324034 
173.00092703083283 0.24406430873785104 
174.00092703083283 0.17775830111002855 
175.00092703083283 0.18875046822785307 
176.00092703083283 0.12993771136683185 
177.00092703083283 0.0967697181267443 
178.00092703083283 0.06642396994650542 
179.00092703083283 0.03842514796238967 
180.00092703083283 0.033731318502738694 
181.00092703083283 0.028752826837175626 
182.00092703083283 0.025914914398315544 
183.00092703083283 0.022852091199114414 
184.00092703083283 0.020348728048812027 
185.00092703083283 0.01855625716734979 
186.00092703083283 0.016625897834232982 
187.00092703083283 0.015382440808296118 
188.00092703083283 0.01333566724071988 
189.00092703083283 0.012198180803934967 
190.00092703083283 0.010956274476504326 
191.00092703083283 0.00939411631949531 
192.00092703083283 0.008442475325092269 
193.00092703083283 0.0075759278214898125 
194.00092703083283 0.5382473578241516 
195.00092703083283 1.6013030303090503 
196.00092703083283 3.727936502760892 
197.00092703083283 3.1954604076440782 
198.00092703083283 5.854176631880306 
199.00092703083283 7.98094392506694 
200.00092703083283 7.9805459372833205 
201.00092703083283 9.044037881150459 
202.00092703083283 8.512023781402302 
203.00092703083283 10.107207641821951 
204.00092703083283 7.980019351224389 
205.00092703083283 12.766118409654656 
206.00092703083283 10.638814085490363 
207.00092703083283 10.63881389197156 
208.00092703083283 17.02039752616983 
209.00092703083283 12.234148786106935 
210.00092703083283 13.829544597896989 
211.00092703083283 11.702314727511252 
212.00092703083283 12.765922120566897 
213.00092703083283 10.638707334455633 
214.00092703083283 13.297725817094744 
215.00092703083283 11.170511030983558 
216.00092703083283 14.893136906678196 
217.00092703083283 11.702314727511276 
218.00092703083283 15.424940603206233 
219.00092703083283 15.956744299733998 
220.00092703083283 11.170511030983555 
221.00092703083283 19.147566478901126 
222.00092703083283 13.297725817094788 
223.00092703083283 14.361333210150477 
224.00092703083283 15.956744299733945 
225.00092703083283 14.893136906678427 
226.00092703083283 17.020351692789653 
227.00092703083283 14.89313690667841 
228.00092703083283 19.14756647890108 
229.00092703083283 14.361333210150416 
230.00092703083283 18.083959085845393 
231.00092703083283 17.02035169278963 
232.00092703083283 17.552155389317512 
233.00092703083283 20.74297756848454 
234.00092703083283 16.488547996261882 
235.00092703083283 18.08395908584537 
236.00092703083283 20.211173871956603 
237.00092703083283 15.956744299733842 
238.00092703083283 18.615762782373054 
239.00092703083283 17.552155389317775 
240.00092703083283 18.083959085845116 
241.00092703083283 18.083959085845812 
242.00092703083283 15.956744299734163 
243.00092703083283 12.23411842403934 
244.00092703083283 19.679370175429288 
245.00092703083283 13.297725817094669 
246.00092703083283 15.956744299734199 
247.00092703083283 15.95674429973421 
248.00092703083283 12.2341184240393 
249.00092703083283 13.829529513622715 
250.00092703083283 10.106903637927664 
251.00092703083283 7.979688851816671 
252.00092703083283 7.979688851816559 
253.00092703083283 6.916081458760846 
254.00092703083283 2.6616518865381824 
255.00092703083283 5.32067036917741 
256.0009270308328 3.725259279593774 
257.0009270308328 2.1298481900102733 
258.0009270308328 3.193455583066048 
259.0009270308328 1.5980444934824958 
260.0009270308328 1.0662407969546241 
261.0009270308328 0.5344371004267972 
262.0009270308328 0.002633403898972407 
263.0009270308328 0.002633403898972407 
264.0009270308328 0.002633403898972407 
265.0009270308328 0.002633403898972407 
266.0009270308328 0.002633403898972407 
267.0009270308328 0.002633403898972407 
268.0009270308328 0.002633403898972407 
269.0009270308328 0.002633403898972407 
270.0009270308328 0.002633403898972407 
271.0009270308328 0.002633403898972407 
272.0009270308328 0.002633403898972407 
273.0009270308328 0.002633403898972407 
274.0009270308328 0.002633403898972407 
275.0009270308328 0.002633403898972407 
276.0009270308328 0.002633403898972407 
277.0009270308328 0.002633403898972407 
278.0009270308328 0.002633403898972407 
279.0009270308328 0.002633403898972407 
280.0009270308328 0.002633403898972407 
281.0009270308328 0.002633403898972407 
282.0009270308328 0.002633403898972407 
283.0009270308328 0.002633403898972407 
284.0009270308328 0.002633403898972407 
285.0009270308328 0.002633403898972407 
286.0009270308328 0.002633403898972407 
287.0009270308328 0.002633403898972407 
288.0009270308328 0.002633403898972407 
289.0009270308328 0.002633403898972407 
290.0009270308328 0.002633403898972407 
291.0009270308328 0.002633403898972407 
292.0009270308328 0.002633403898972407 
293.0009270308328 0.002633403898972407 
294.0009270308328 0.002633403898972407 
295.0009270308328 0.002633403898972407 
296.0009270308328 0.002633403898972407 
297.0009270308328 0.002633403898972407 
298.0009270308328 0.002633403898972407 
299.0009270308328 0.002633403898972407 
300.0009270308328 0.002633403898972407 
301.0009270308328 0.002633403898972407 
302.0009270308328 0.002633403898972407 
303.0009270308328 0.002633403898972407 
304.0009270308328 0.002633403898972407 
305.0009270308328 0.002633403898972407 
306.0009270308328 0.002633403898972407 
307.0009270308328 0.002633403898972407 
308.0009270308328 0.002633403898972407 
309.0009270308328 0.002633403898972407 
310.0009270308328 0.002633403898972407 
311.0009270308328 0.002633403898972407 
312.0009270308328 0.002633403898972407 
313.0009270308328 0.002633403898972407 
314.0009270308328 0.002633403898972407 
315.0009270308328 0.002633403898972407 
316.0009270308328 0.002633403898972407 
317.0009270308328 0.002633403898972407 
318.0009270308328 0.002633403898972407 
319.0009270308328 0.002633403898972407 
320.0009270308328 0.002633403898972407 
321.0009270308328 0.002633403898972407 
322.0009270308328 0.002633403898972407 
323.0009270308328 0.002633403898972407 
324.0009270308328 0.002633403898972407 
325.0009270308328 0.002633403898972407 
326.0009270308328 0.002633403898972407 
327.0009270308328 0.002633403898972407 
328.0009270308328 0.002633403898972407 
329.0009270308328 0.002633403898972407 
330.0009270308328 0.002633403898972407 
331.0009270308328 0.002633403898972407 
332.0009270308328 0.002633403898972407 
333.0009270308328 0.002633403898972407 
334.0009270308328 0.002633403898972407 
335.0009270308328 0.002633403898972407 
336.0009270308328 0.002633403898972407 
337.0009270308328 0.002633403898972407 
338.0009270308328 0.002633403898972407 
339.0009270308328 0.002633403898972407 
340.0009270308328 0.002633403898972407 
341.0009270308328 0.002633403898972407 
342.0009270308328 0.002633403898972407 
343.0009270308328 0.002633403898972407 
344.0009270308328 0.002633403898972407 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T11:53+0000 #
#####################################################################
