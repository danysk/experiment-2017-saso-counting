#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T22:08+0000 #
#####################################################################
# random = 6.0, targets = 1.0, density = 0.1591589639397027, gridShift = 1.0, perturbation = 0.5, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0115592716150654 NaN 
2.0115592716150656 1.0 
3.0115592716150656 1.0 
4.011559271615066 1.0 
5.011559271615066 1.0 
6.011559271615066 0.9666666666666667 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T22:08+0000 #
#####################################################################
