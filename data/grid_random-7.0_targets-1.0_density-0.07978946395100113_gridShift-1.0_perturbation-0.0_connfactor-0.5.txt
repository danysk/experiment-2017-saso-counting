#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T14:55+0000 #
#####################################################################
# random = 7.0, targets = 1.0, density = 0.07978946395100113, gridShift = 1.0, perturbation = 0.0, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0724373403975138 NaN 
2.0724373403975136 1.0 
3.0724373403975136 1.0 
4.072437340397514 0.9625 
5.072437340397514 0.8 
6.072437340397514 0.65 
7.072437340397514 0.4749999999999999 
8.072437340397514 0.2874999999999999 
9.072437340397514 0.1875 
10.072437340397514 0.125 
11.072437340397514 0.07500000000000001 
12.072437340397514 0.037500000000000006 
13.072437340397514 0.0 
14.072437340397514 0.0 
15.072437340397514 0.0 
16.072437340397514 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T14:55+0000 #
#####################################################################
