#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T16:41+0000 #
#####################################################################
# random = 18.0, targets = 31.622776601683793, density = 0.04, gridShift = 1.0, perturbation = 0.5, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0067713218599703 NaN 
2.0067713218599703 1.0 
3.0067713218599703 1.001766080181575 
4.00677132185997 0.9971345063035165 
5.00677132185997 0.8966258374496622 
6.00677132185997 0.741117232807068 
7.00677132185997 0.7318306898585457 
8.00677132185997 0.7575090220157851 
9.00677132185997 0.7645616451458824 
10.00677132185997 0.7630177871865297 
11.00677132185997 0.7630177871865297 
12.00677132185997 0.7630177871865297 
13.00677132185997 0.7630177871865297 
14.00677132185997 0.7630177871865297 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T16:41+0000 #
#####################################################################
