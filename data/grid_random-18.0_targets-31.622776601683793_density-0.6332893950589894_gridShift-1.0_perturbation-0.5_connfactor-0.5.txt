#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T16:44+0000 #
#####################################################################
# random = 18.0, targets = 31.622776601683793, density = 0.6332893950589894, gridShift = 1.0, perturbation = 0.5, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0067713218599703 NaN 
2.0067713218599703 0.9993083827421848 
3.0067713218599703 1.0002354773575433 
4.00677132185997 0.9954778149977628 
5.00677132185997 0.9880035375205424 
6.00677132185997 0.9873242659417395 
7.00677132185997 0.964556225695504 
8.00677132185997 0.966163614492919 
9.00677132185997 0.9604955571236808 
10.00677132185997 0.9534319338890418 
11.00677132185997 0.9292116736916366 
12.00677132185997 0.9198400773148663 
13.00677132185997 0.9140765199586959 
14.00677132185997 0.8851389614814317 
15.00677132185997 0.8809108958120464 
16.006771321859972 0.8543504133401482 
17.006771321859972 0.8629210487113828 
18.006771321859972 0.8467011007833181 
19.006771321859972 0.838198380975893 
20.006771321859972 0.8172563941317701 
21.006771321859972 0.8018666931172856 
22.006771321859972 0.7767007209683005 
23.006771321859972 0.7694135936750618 
24.006771321859972 0.7513081067917358 
25.006771321859972 0.7463353703840165 
26.006771321859972 0.7314561302574583 
27.006771321859972 0.7246184186195034 
28.006771321859972 0.7193132253955252 
29.006771321859972 0.705018878281794 
30.006771321859972 0.6876409758215906 
31.006771321859972 0.6844845766881948 
32.00677132185997 0.6441688937375882 
33.00677132185997 0.6449691343607196 
34.00677132185997 0.6344325011559957 
35.00677132185997 0.6165142346158707 
36.00677132185997 0.606587167172006 
37.00677132185997 0.5848958534418112 
38.00677132185997 0.5883536905462545 
39.00677132185997 0.5686587689560794 
40.00677132185997 0.5556161119720079 
41.00677132185997 0.5573843290357922 
42.00677132185997 0.5292020268672136 
43.00677132185997 0.5424575107765601 
44.00677132185997 0.5438249678046316 
45.00677132185997 0.5497706855239521 
46.00677132185997 0.5482393514354208 
47.00677132185997 0.5474843662557771 
48.00677132185997 0.5521376400822262 
49.00677132185997 0.5499759632363913 
50.00677132185997 0.5516836458733728 
51.00677132185997 0.5526257153585297 
52.00677132185997 0.5515561775711746 
53.00677132185997 0.5549272757588982 
54.00677132185997 0.5565193802109353 
55.00677132185997 0.5587361414145542 
56.00677132185997 0.5580726114100963 
57.00677132185997 0.5585642286679116 
58.00677132185997 0.5582117924713433 
59.00677132185997 0.5582117924713433 
60.00677132185997 0.5580355743730593 
61.00677132185997 0.5580355743730593 
62.00677132185997 0.5580355743730593 
63.00677132185997 0.5580355743730593 
64.00677132185997 0.5580355743730593 
65.00677132185997 0.5580355743730593 
66.00677132185997 0.5580355743730593 
67.00677132185997 0.5580355743730593 
68.00677132185997 0.5580355743730593 
69.00677132185997 0.5580355743730593 
70.00677132185997 0.5580355743730593 
71.00677132185997 0.5580355743730593 
72.00677132185997 0.5580355743730593 
73.00677132185997 0.5580355743730593 
74.00677132185997 0.5580355743730593 
75.00677132185997 0.5580355743730593 
76.00677132185997 0.5580355743730593 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T16:44+0000 #
#####################################################################
