#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T07:27+0000 #
#####################################################################
# random = 17.0, targets = 10.0, density = 0.04, gridShift = 0.0, perturbation = 0.5, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0045647570182237 NaN 
2.0045647570182235 1.0521875 
3.0045647570182235 1.091875 
4.0045647570182235 1.0982812499999999 
5.0045647570182235 1.05375 
6.0045647570182235 0.9896918402777778 
7.0045647570182235 0.8933463541666667 
8.004564757018223 0.8152907986111112 
9.004564757018223 0.7615581597222224 
10.004564757018223 0.7673784722222223 
11.004564757018223 0.7977690972222222 
12.004564757018223 0.7887500000000001 
13.004564757018223 0.7990625 
14.004564757018223 0.81 
15.004564757018223 0.81 
16.004564757018223 0.81 
17.004564757018223 0.81 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T07:27+0000 #
#####################################################################
