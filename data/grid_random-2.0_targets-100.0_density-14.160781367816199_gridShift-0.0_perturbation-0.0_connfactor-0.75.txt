#####################################################################
# Alchemist log file - simulation started at: 2017-05-21T17:17+0000 #
#####################################################################
# random = 2.0, targets = 100.0, density = 14.160781367816199, gridShift = 0.0, perturbation = 0.0, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0002668521400264 NaN 
2.0002668521400264 1.0012073800131491 
3.0002668521400264 1.0026433984403536 
4.000266852140026 1.003358518244576 
5.000266852140026 1.0037956146358389 
6.000266852140026 1.0018117028270874 
7.000266852140026 1.00229748018482 
8.000266852140026 1.0009873411133026 
9.000266852140026 1.0003731307984514 
10.000266852140026 0.9978920109211776 
11.000266852140026 0.9959062737416904 
12.000266852140026 0.9937222957301483 
13.000266852140026 0.9904710798816568 
14.000266852140026 0.9882656763642341 
15.000266852140026 0.9851929948316166 
16.000266852140026 0.9824006150010958 
17.000266852140026 0.9803605125465703 
18.000266852140026 0.9772856440390094 
19.000266852140026 0.9751461346701731 
20.000266852140026 0.9729088784790708 
21.000266852140026 0.9712107613777485 
22.000266852140026 0.9687820645226094 
23.000266852140026 0.966954101833589 
24.000266852140026 0.9651669374132515 
25.000266852140026 0.9629378702790562 
26.000266852140026 0.9611227550405436 
27.000266852140026 0.9584562267148805 
28.000266852140026 0.9563978340273211 
29.000266852140026 0.9538788776572431 
30.000266852140026 0.9524001976952297 
31.000266852140026 0.949009068412594 
32.00026685214003 0.9471260044561326 
33.00026685214003 0.9440564550003652 
34.00026685214003 0.9409379684418147 
35.00026685214003 0.9390575201804368 
36.00026685214003 0.9348160653627 
37.00026685214003 0.9328118740412009 
38.00026685214003 0.9285306710680108 
39.00026685214003 0.9265434098546277 
40.00026685214003 0.9227287521002265 
41.00026685214003 0.9211480472459639 
42.00026685214003 0.9179089318978743 
43.00026685214003 0.9149905261523851 
44.00026685214003 0.9100188490759004 
45.00026685214003 0.908755502593323 
46.00026685214003 0.9043333100482138 
47.00026685214003 0.9011313152896486 
48.00026685214003 0.9005365763569289 
49.00026685214003 0.8959916968003506 
50.00026685214003 0.8947365320695448 
51.00026685214003 0.894010828950252 
52.00026685214003 0.8925445567609029 
53.00026685214003 0.8907050441960698 
54.00026685214003 0.889269719756739 
55.00026685214003 0.8882902330338227 
56.00026685214003 0.886731966359851 
57.00026685214003 0.885241527412521 
58.00026685214003 0.8851257912374899 
59.00026685214003 0.881002266874863 
60.00026685214003 0.8798713789356417 
61.00026685214003 0.8770331023084228 
62.00026685214003 0.8743820805025934 
63.00026685214003 0.8740716601833589 
64.00026685214003 0.8717739028599606 
65.00026685214003 0.867523091989919 
66.00026685214003 0.8675926291182701 
67.00026685214003 0.8642253337533785 
68.00026685214003 0.8634494087405947 
69.00026685214003 0.8598196467053839 
70.00026685214003 0.8594458703155817 
71.00026685214003 0.8553671150193587 
72.00026685214003 0.8525769349477682 
73.00026685214003 0.8523015126196216 
74.00026685214003 0.8478700064833077 
75.00026685214003 0.8446731148184673 
76.00026685214003 0.8420953534772445 
77.00026685214003 0.8382968336803274 
78.00026685214003 0.8325585091131565 
79.00026685214003 0.8320311984074805 
80.00026685214003 0.8246975308641975 
81.00026685214003 0.8211298999196435 
82.00026685214003 0.8193048341734239 
83.00026685214003 0.8116804975710424 
84.00026685214003 0.8084305400321425 
85.00026685214003 0.803882935660019 
86.00026685214003 0.7986988553765797 
87.00026685214003 0.7944920469902843 
88.00026685214003 0.7899791643838119 
89.00026685214003 0.7855695321060707 
90.00026685214003 0.7807793036379576 
91.00026685214003 0.7764168337716415 
92.00026685214003 0.768780266089561 
93.00026685214003 0.764614874625612 
94.00026685214003 0.7573177131273285 
95.00026685214003 0.7524489905215868 
96.00026685214003 0.7456424702315728 
97.00026685214003 0.7407270125648332 
98.00026685214003 0.7337731773686903 
99.00026685214003 0.7271141249360801 
100.00026685214003 0.7192048875922273 
101.00026685214003 0.710678043045511 
102.00026685214003 0.7022754104573014 
103.00026685214003 0.695515124826503 
104.00026685214003 0.6916021116407336 
105.00026685214003 0.6778728153079115 
106.00026685214003 0.7104655955511725 
107.00026685214003 0.6780950466615532 
108.00026685214003 0.7036397705274308 
109.00026685214003 0.7023959072978304 
110.00026685214003 0.6659362677149537 
111.00026685214003 0.6805642523193806 
112.00026685214003 0.6666085383336986 
113.00026685214003 0.6350283334246474 
114.00026685214003 0.6339463839579226 
115.00026685214003 0.6206378008802689 
116.00026685214003 0.6095606696069837 
117.00026685214003 0.5921063152896487 
118.00026685214003 0.5878991370808677 
119.00026685214003 0.5955974358974357 
120.00026685214003 0.5600893262838774 
121.00026685214003 0.5716645162174008 
122.00026685214003 0.5689255967382569 
123.00026685214003 0.5329249068595222 
124.00026685214003 0.5401904955621302 
125.00026685214003 0.5320304514573745 
126.00026685214003 0.501763156549054 
127.00026685214003 0.5038215232120682 
128.00026685214004 0.4916203717400834 
129.00026685214004 0.4814720117430054 
130.00026685214004 0.46950634907590055 
131.00026685214004 0.4691424003762144 
132.00026685214004 0.4484845907297831 
133.00026685214004 0.44221246027832584 
134.00026685214004 0.44307675049309675 
135.00026685214004 0.41805745626050117 
136.00026685214004 0.41998967738695286 
137.00026685214004 0.4054800236503763 
138.00026685214004 0.3972378634304915 
139.00026685214004 0.39861654750164355 
140.00026685214004 0.37977392842793484 
141.00026685214004 0.379068257359924 
142.00026685214004 0.36807958305939087 
143.00026685214004 0.3637573886879977 
144.00026685214004 0.357889799291402 
145.00026685214004 0.3482924592738696 
146.00026685214004 0.34447860599751634 
147.00026685214004 0.3349345701840894 
148.00026685214004 0.3286850861092849 
149.00026685214004 0.3221458785338593 
150.00026685214004 0.31833839168310324 
151.00026685214004 0.3102816705018629 
152.00026685214004 0.3078252351340493 
153.00026685214004 0.29792000830959176 
154.00026685214004 0.2962341236576815 
155.00026685214004 0.2924005729965667 
156.00026685214004 0.2883269828877201 
157.00026685214004 0.27667696051574264 
158.00026685214004 0.27843139701585223 
159.00026685214004 0.27049269258163494 
160.00026685214004 0.2696471236028929 
161.00026685214004 0.26564800752428963 
162.00026685214004 0.26128270920081825 
163.00026685214004 0.2622892943239098 
164.00026685214004 0.2583046223244942 
165.00026685214004 0.25833672519906503 
166.00026685214004 0.2539441166264885 
167.00026685214004 0.25374511332091465 
168.00026685214004 0.2507333830995691 
169.00026685214004 0.2494363060669151 
170.00026685214004 0.24473157279567545 
171.00026685214004 0.2451195101906641 
172.00026685214004 0.24236460479216898 
173.00026685214004 0.24196201786105642 
174.00026685214004 0.23941316248447672 
175.00026685214004 0.23866283923223033 
176.00026685214004 0.2378137199576303 
177.00026685214004 0.2376592957849369 
178.00026685214004 0.2367932085068304 
179.00026685214004 0.2362359458324203 
180.00026685214004 0.23581488101760548 
181.00026685214004 0.23549283594491938 
182.00026685214004 0.23458481444955812 
183.00026685214004 0.23421611330265185 
184.00026685214004 0.23378892084885683 
185.00026685214004 0.23333885281978242 
186.00026685214004 0.2333618420812332 
187.00026685214004 0.23304495991306898 
188.00026685214004 0.23250494146760184 
189.00026685214004 0.232175105924465 
190.00026685214004 0.23165814796551987 
191.00026685214004 0.23152433249324283 
192.00026685214004 0.2311280768317628 
193.00026685214004 0.23105846436920166 
194.00026685214004 0.23082657151727676 
195.00026685214004 0.23080670611439852 
196.00026685214004 0.2307078238001316 
197.00026685214004 0.23069240722477913 
198.00026685214004 0.230624360344072 
199.00026685214004 0.2306174848418439 
200.00026685214004 0.23056722961867204 
201.00026685214004 0.23053465236686402 
202.00026685214004 0.23050599523339918 
203.00026685214004 0.23046662557527953 
204.00026685214004 0.23043339497041432 
205.00026685214004 0.23041757478632488 
206.00026685214004 0.23041265614727163 
207.00026685214004 0.23039700854700865 
208.00026685214004 0.23040158201840905 
209.00026685214004 0.23039068047337288 
210.00026685214004 0.2304000000000001 
211.00026685214004 0.2304000000000001 
212.00026685214004 0.2304000000000001 
213.00026685214004 0.2304000000000001 
214.00026685214004 0.2304000000000001 
215.00026685214004 0.2304000000000001 
216.00026685214004 0.2304000000000001 
217.00026685214004 0.2304000000000001 
218.00026685214004 0.2304000000000001 
219.00026685214004 0.2304000000000001 
220.00026685214004 0.2304000000000001 
221.00026685214004 0.2304000000000001 
222.00026685214004 0.2304000000000001 
223.00026685214004 0.2304000000000001 
224.00026685214004 0.2304000000000001 
225.00026685214004 0.2304000000000001 
226.00026685214004 0.2304000000000001 
227.00026685214004 0.2304000000000001 
228.00026685214004 0.2304000000000001 
229.00026685214004 0.2304000000000001 
230.00026685214004 0.2304000000000001 
231.00026685214004 0.2304000000000001 
232.00026685214004 0.2304000000000001 
233.00026685214004 0.2304000000000001 
234.00026685214004 0.2304000000000001 
235.00026685214004 0.2304000000000001 
236.00026685214004 0.2304000000000001 
237.00026685214004 0.2304000000000001 
238.00026685214004 0.2304000000000001 
239.00026685214004 0.2304000000000001 
240.00026685214004 0.2304000000000001 
241.00026685214004 0.2304000000000001 
242.00026685214004 0.2304000000000001 
243.00026685214004 0.2304000000000001 
244.00026685214004 0.2304000000000001 
245.00026685214004 0.2304000000000001 
246.00026685214004 0.2304000000000001 
247.00026685214004 0.2304000000000001 
248.00026685214004 0.2304000000000001 
249.00026685214004 0.2304000000000001 
250.00026685214004 0.2304000000000001 
251.00026685214004 0.2304000000000001 
252.00026685214004 0.2304000000000001 
253.00026685214004 0.2304000000000001 
254.00026685214004 0.2304000000000001 
255.00026685214004 0.2304000000000001 
256.00026685214004 0.2304000000000001 
257.00026685214004 0.2304000000000001 
258.00026685214004 0.2304000000000001 
259.00026685214004 0.2304000000000001 
260.00026685214004 0.2304000000000001 
261.00026685214004 0.2304000000000001 
262.00026685214004 0.2304000000000001 
263.00026685214004 0.2304000000000001 
264.00026685214004 0.2304000000000001 
265.00026685214004 0.2304000000000001 
266.00026685214004 0.2304000000000001 
267.00026685214004 0.2304000000000001 
268.00026685214004 0.2304000000000001 
269.00026685214004 0.2304000000000001 
270.00026685214004 0.2304000000000001 
271.00026685214004 0.2304000000000001 
272.00026685214004 0.2304000000000001 
273.00026685214004 0.2304000000000001 
274.00026685214004 0.2304000000000001 
275.00026685214004 0.2304000000000001 
276.00026685214004 0.2304000000000001 
277.00026685214004 0.2304000000000001 
278.00026685214004 0.2304000000000001 
279.00026685214004 0.2304000000000001 
280.00026685214004 0.2304000000000001 
281.00026685214004 0.2304000000000001 
282.00026685214004 0.2304000000000001 
283.00026685214004 0.2304000000000001 
284.00026685214004 0.2304000000000001 
285.00026685214004 0.2304000000000001 
286.00026685214004 0.2304000000000001 
287.00026685214004 0.2304000000000001 
288.00026685214004 0.2304000000000001 
289.00026685214004 0.2304000000000001 
290.00026685214004 0.2304000000000001 
291.00026685214004 0.2304000000000001 
292.00026685214004 0.2304000000000001 
293.00026685214004 0.2304000000000001 
294.00026685214004 0.2304000000000001 
295.00026685214004 0.2304000000000001 
296.00026685214004 0.2304000000000001 
#####################################################################
# End of data export. Simulation finished at: 2017-05-21T17:36+0000 #
#####################################################################
