#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T15:05+0000 #
#####################################################################
# random = 4.0, targets = 3.1622776601683795, density = 1.7841466528270753, gridShift = 1.0, perturbation = 0.5, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0035718759004084 NaN 
2.003571875900408 1.0903351410664839 
3.003571875900408 1.1966046402485486 
4.003571875900408 1.285855051333155 
5.003571875900408 1.3082495784145545 
6.003571875900408 1.2689940948540503 
7.003571875900408 1.321756689603026 
8.003571875900409 1.1661966653623697 
9.003571875900409 1.0960604233762379 
10.003571875900409 1.036285527880028 
11.003571875900409 1.044891186679273 
12.003571875900409 1.0112227440531352 
13.003571875900409 0.9663965391077183 
14.003571875900409 0.9152589919197367 
15.003571875900409 0.8682543443785878 
16.00357187590041 0.8398107243665197 
17.00357187590041 0.7799699663443102 
18.00357187590041 0.7076249931023436 
19.00357187590041 0.6440416639693456 
20.00357187590041 0.6013764541492798 
21.00357187590041 0.5476458434360705 
22.00357187590041 0.5222216383469649 
23.00357187590041 0.48369046469438226 
24.00357187590041 0.46008459579045224 
25.00357187590041 0.4576527339829619 
26.00357187590041 0.448999601118299 
27.00357187590041 0.45551849122309196 
28.00357187590041 0.45558245456924606 
29.00357187590041 0.4701814285268981 
30.00357187590041 0.48153792580771193 
31.00357187590041 0.4957922900470107 
32.003571875900406 0.5088091874250982 
33.003571875900406 0.5276544492193348 
34.003571875900406 0.5313746787148506 
35.003571875900406 0.5481287782638724 
36.003571875900406 0.5785942196354257 
37.003571875900406 0.6028044445989651 
38.003571875900406 0.6150981771895044 
39.003571875900406 0.6329996724143473 
40.003571875900406 0.6807034974132703 
41.003571875900406 0.6907755371621385 
42.003571875900406 0.7058405162707846 
43.003571875900406 0.7172100674712164 
44.003571875900406 0.7286070327628182 
45.003571875900406 0.7296173665825131 
46.003571875900406 0.7311576688993572 
47.003571875900406 0.73194217592947 
48.003571875900406 0.7317111413315963 
49.003571875900406 0.7276489318210392 
50.003571875900406 0.7440624006055656 
51.003571875900406 1.0677586738208944 
52.003571875900406 1.607155853580511 
53.003571875900406 1.612861507712369 
54.003571875900406 1.9475292778768691 
55.003571875900406 2.3346380255500714 
56.003571875900406 1.1145673301633743 
57.003571875900406 2.7443071879637295 
58.003571875900406 2.7218030561159847 
59.003571875900406 2.7019200894094957 
60.003571875900406 2.700170466783251 
61.003571875900406 3.0396182767048945 
62.003571875900406 3.754157760527819 
63.003571875900406 2.9843935830866606 
64.0035718759004 3.838797031678837 
65.0035718759004 4.348686301616774 
66.0035718759004 4.376711444404762 
67.0035718759004 4.719470721563521 
68.0035718759004 3.1514263334208583 
69.0035718759004 4.2114529811414885 
70.0035718759004 4.2274703722521 
71.0035718759004 4.292517360297624 
72.0035718759004 2.7613692004235393 
73.0035718759004 4.0490984843000115 
74.0035718759004 3.0523079778890505 
75.0035718759004 1.7271171533102934 
76.0035718759004 2.1518581116933397 
77.0035718759004 1.016246149499652 
78.0035718759004 1.0383795402538043 
79.0035718759004 0.5648166142689703 
80.0035718759004 0.29642623728484363 
81.0035718759004 0.35273831005087586 
82.0035718759004 0.48344390624783734 
83.0035718759004 0.1646619782097918 
84.0035718759004 0.5425117460392199 
85.0035718759004 0.5593299035313428 
86.0035718759004 0.5574033869647065 
87.0035718759004 0.5612737838342996 
88.0035718759004 0.636384501585291 
89.0035718759004 0.8044727478380864 
90.0035718759004 0.6274176170542837 
91.0035718759004 0.8062945623829918 
92.0035718759004 0.9489925746966882 
93.0035718759004 0.9230153117722484 
94.0035718759004 1.0759434954531655 
95.0035718759004 0.6559818698397977 
96.0035718759004 0.908116894721469 
97.0035718759004 0.9077518941660221 
98.0035718759004 0.9431959319252475 
99.0035718759004 0.5561271752047101 
100.0035718759004 0.8631322766300258 
101.0035718759004 0.6630257826985149 
102.0035718759004 0.33146183323855716 
103.0035718759004 0.4592652870592196 
104.0035718759004 0.1982865369266313 
105.0035718759004 0.19827348300701264 
106.0035718759004 0.06785067949088051 
107.0035718759004 0.002633403898972407 
108.0035718759004 0.002633403898972407 
109.0035718759004 0.002633403898972407 
110.0035718759004 0.002633403898972407 
111.0035718759004 0.002633403898972407 
112.0035718759004 0.002633403898972407 
113.0035718759004 0.002633403898972407 
114.0035718759004 0.002633403898972407 
115.0035718759004 0.002633403898972407 
116.0035718759004 0.002633403898972407 
117.0035718759004 0.002633403898972407 
118.0035718759004 0.002633403898972407 
119.0035718759004 0.002633403898972407 
120.0035718759004 0.002633403898972407 
121.0035718759004 0.002633403898972407 
122.0035718759004 0.002633403898972407 
123.0035718759004 0.002633403898972407 
124.0035718759004 0.002633403898972407 
125.0035718759004 0.002633403898972407 
126.0035718759004 0.002633403898972407 
127.0035718759004 0.002633403898972407 
128.0035718759004 0.002633403898972407 
129.0035718759004 0.002633403898972407 
130.0035718759004 0.002633403898972407 
131.0035718759004 0.002633403898972407 
132.0035718759004 0.002633403898972407 
133.0035718759004 0.002633403898972407 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T15:05+0000 #
#####################################################################
