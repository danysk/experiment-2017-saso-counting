#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T12:41+0000 #
#####################################################################
# random = 18.0, targets = 1.0, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.1735073816495318 NaN 
2.173507381649532 4.875 
3.173507381649532 5.099392361111111 
4.173507381649532 4.360243055555555 
5.173507381649532 4.8984375 
6.173507381649532 2.0746527777777777 
7.173507381649532 2.1566840277777777 
8.173507381649532 0.7769097222222221 
9.173507381649532 0.7526041666666665 
10.173507381649532 0.30251736111111105 
11.173507381649532 0.09895833333333331 
12.173507381649532 0.140625 
13.173507381649532 0.0546875 
14.173507381649532 0.05078125 
15.173507381649532 0.0078125 
16.173507381649532 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T12:41+0000 #
#####################################################################
