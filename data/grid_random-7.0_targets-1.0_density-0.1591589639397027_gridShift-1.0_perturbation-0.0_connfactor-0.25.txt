#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T14:59+0000 #
#####################################################################
# random = 7.0, targets = 1.0, density = 0.1591589639397027, gridShift = 1.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0407271777208422 NaN 
2.040727177720842 1.0 
3.040727177720842 1.0 
4.040727177720842 1.0 
5.040727177720842 1.0 
6.040727177720842 1.0 
7.040727177720842 1.0 
8.040727177720843 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T14:59+0000 #
#####################################################################
