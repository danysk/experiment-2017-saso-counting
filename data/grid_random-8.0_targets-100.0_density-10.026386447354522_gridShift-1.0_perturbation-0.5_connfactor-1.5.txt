#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T22:12+0000 #
#####################################################################
# random = 8.0, targets = 100.0, density = 10.026386447354522, gridShift = 1.0, perturbation = 0.5, connfactor = 1.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0009270308328329 NaN 
2.000927030832833 1.008801866359447 
3.000927030832833 1.013747430875576 
4.000927030832833 1.0196032418074756 
5.000927030832833 1.0238035682283666 
6.000927030832833 1.0211818375576036 
7.000927030832833 1.02754770609319 
8.000927030832834 1.0310390687403994 
9.000927030832834 1.0424240271377367 
10.000927030832834 1.0457878328213006 
11.000927030832834 1.0563334856630824 
12.000927030832834 1.058114658858167 
13.000927030832834 1.06667952124936 
14.000927030832834 1.0722732565284179 
15.000927030832834 1.0802490124167947 
16.000927030832834 1.0789989868151562 
17.000927030832834 1.0849261053507424 
18.000927030832834 1.0904033192524323 
19.000927030832834 1.08997883640553 
20.000927030832834 1.089378580389145 
21.000927030832834 1.0934836347926267 
22.000927030832834 1.0805979992319508 
23.000927030832834 1.0881045442908346 
24.000927030832834 1.0803791679467487 
25.000927030832834 1.0878874295954941 
26.000927030832834 1.0794104614695341 
27.000927030832834 1.0844978725038403 
28.000927030832834 1.094035721326165 
29.000927030832834 1.10372330453149 
30.000927030832834 1.1145589880952382 
31.000927030832834 1.1258800070404507 
32.000927030832834 1.1375103405017921 
33.000927030832834 1.1611672324628777 
34.000927030832834 1.1786821825396825 
35.000927030832834 1.2091101376088071 
36.000927030832834 1.231361769713262 
37.000927030832834 1.2350424750384024 
38.000927030832834 1.269706298643113 
39.000927030832834 1.3140123988735284 
40.000927030832834 1.3266386239119305 
41.000927030832834 1.3519293932411673 
42.000927030832834 1.3909269674859195 
43.000927030832834 1.393451817716334 
44.000927030832834 1.4367675825652837 
45.000927030832834 1.465759892473118 
46.000927030832834 1.5294615584997442 
47.000927030832834 1.5358107200460827 
48.000927030832834 1.5546621806195597 
49.000927030832834 1.6111203513824888 
50.000927030832834 1.61982834421403 
51.000927030832834 1.653061720430108 
52.000927030832834 1.6571518554787505 
53.000927030832834 1.7052978840245778 
54.000927030832834 1.6697285259856633 
55.000927030832834 1.6974511776753713 
56.000927030832834 1.7345131573220685 
57.000927030832834 1.7471053181003586 
58.000927030832834 1.7296347510240657 
59.000927030832834 1.750455120327701 
60.000927030832834 1.7627022919866873 
61.000927030832834 1.7397793644393242 
62.000927030832834 1.732893549667179 
63.000927030832834 1.7597703737839223 
64.00092703083283 1.7509516404249874 
65.00092703083283 1.7552750576036869 
66.00092703083283 1.7441439375320023 
67.00092703083283 1.7573771434971839 
68.00092703083283 1.7422441730670764 
69.00092703083283 1.761848838965694 
70.00092703083283 1.7428401638504867 
71.00092703083283 1.7472094322836664 
72.00092703083283 1.7564137115975422 
73.00092703083283 1.8094736290322582 
74.00092703083283 1.8170085599078345 
75.00092703083283 1.8935351088069636 
76.00092703083283 1.902919592293907 
77.00092703083283 1.949261654505889 
78.00092703083283 2.075577338709678 
79.00092703083283 2.096233297491039 
80.00092703083283 2.1170037435995903 
81.00092703083283 2.257803259728623 
82.00092703083283 2.1267838645673325 
83.00092703083283 2.340943128520226 
84.00092703083283 2.2953159158986183 
85.00092703083283 2.430690206733231 
86.00092703083283 2.4472254832309273 
87.00092703083283 2.4683508320532517 
88.00092703083283 2.586444302995392 
89.00092703083283 2.637657161418331 
90.00092703083283 2.6023427073732726 
91.00092703083283 2.716204548131081 
92.00092703083283 2.66294093061956 
93.00092703083283 2.7473329838709684 
94.00092703083283 2.7413703052995397 
95.00092703083283 2.809085534434204 
96.00092703083283 2.856249511008705 
97.00092703083283 2.852836910522274 
98.00092703083283 2.8744127796979013 
99.00092703083283 2.9013069348438307 
100.00092703083283 2.8175472951868925 
101.00092703083283 2.9405017095494115 
102.00092703083283 2.7951296703789046 
103.00092703083283 2.876820577316949 
104.00092703083283 2.8813616109831033 
105.00092703083283 2.876809149385561 
106.00092703083283 2.8689439228110603 
107.00092703083283 2.874312866743472 
108.00092703083283 2.926461621223759 
109.00092703083283 2.9702966289042503 
110.00092703083283 2.8929576504096266 
111.00092703083283 2.909039120583717 
112.00092703083283 2.8912054397081417 
113.00092703083283 2.760442163978495 
114.00092703083283 2.825360842293907 
115.00092703083283 2.8173225364823353 
116.00092703083283 2.778088693036354 
117.00092703083283 2.7103156073988734 
118.00092703083283 2.696385543394777 
119.00092703083283 2.616525003840246 
120.00092703083283 2.5958994546850995 
121.00092703083283 2.493165350742448 
122.00092703083283 2.473805225294419 
123.00092703083283 2.3490592044290834 
124.00092703083283 2.361389171786995 
125.00092703083283 2.302962499359959 
126.00092703083283 2.2418824756784437 
127.00092703083283 2.1803265853814646 
128.00092703083283 2.19457098374296 
129.00092703083283 2.1543846025345625 
130.00092703083283 2.120975638760881 
131.00092703083283 2.0993631477214545 
132.00092703083283 2.098932697772658 
133.00092703083283 2.057477235663083 
134.00092703083283 2.038557758576549 
135.00092703083283 2.0532998412698418 
136.00092703083283 2.038367320788531 
137.00092703083283 2.0492691596262165 
138.00092703083283 2.044440147209422 
139.00092703083283 2.0527195986943165 
140.00092703083283 2.0637001248079883 
141.00092703083283 2.0578533294930876 
142.00092703083283 2.054284646057348 
143.00092703083283 2.0646496831797236 
144.00092703083283 2.0526755824372764 
145.00092703083283 2.0657143990015365 
146.00092703083283 2.0589704896313368 
147.00092703083283 2.0622894086021506 
148.00092703083283 2.0569153347414235 
149.00092703083283 2.057655448668715 
150.00092703083283 2.0554139656938046 
151.00092703083283 2.05618563812084 
152.00092703083283 2.052777552483359 
153.00092703083283 2.055733666154634 
154.00092703083283 2.0508169630056323 
155.00092703083283 2.048706000384025 
156.00092703083283 2.0476327988991296 
157.00092703083283 2.045446559779826 
158.00092703083283 2.0449 
159.00092703083283 2.0449 
160.00092703083283 2.0449 
161.00092703083283 2.0449 
162.00092703083283 2.0449 
163.00092703083283 2.0449 
164.00092703083283 2.0449 
165.00092703083283 2.0449 
166.00092703083283 2.0449 
167.00092703083283 2.0449 
168.00092703083283 2.0449 
169.00092703083283 2.0449 
170.00092703083283 2.0449 
171.00092703083283 2.0449 
172.00092703083283 2.0449 
173.00092703083283 2.0449 
174.00092703083283 2.0449 
175.00092703083283 2.0449 
176.00092703083283 2.0449 
177.00092703083283 2.0449 
178.00092703083283 2.0449 
179.00092703083283 2.0449 
180.00092703083283 2.0449 
181.00092703083283 2.0449 
182.00092703083283 2.0449 
183.00092703083283 2.0449 
184.00092703083283 2.0449 
185.00092703083283 2.0449 
186.00092703083283 2.0449 
187.00092703083283 2.0449 
188.00092703083283 2.0449 
189.00092703083283 2.0449 
190.00092703083283 2.0449 
191.00092703083283 2.0449 
192.00092703083283 2.0449 
193.00092703083283 2.0449 
194.00092703083283 2.0449 
195.00092703083283 2.0449 
196.00092703083283 2.0449 
197.00092703083283 2.0449 
198.00092703083283 2.0449 
199.00092703083283 2.0449 
200.00092703083283 2.0449 
201.00092703083283 2.0449 
202.00092703083283 2.0449 
203.00092703083283 2.0449 
204.00092703083283 2.0449 
205.00092703083283 2.0449 
206.00092703083283 2.0449 
207.00092703083283 2.0449 
208.00092703083283 2.0449 
209.00092703083283 2.0449 
210.00092703083283 2.0449 
211.00092703083283 2.0449 
212.00092703083283 2.0449 
213.00092703083283 2.0449 
214.00092703083283 2.0449 
215.00092703083283 2.0449 
216.00092703083283 2.0449 
217.00092703083283 2.0449 
218.00092703083283 2.0449 
219.00092703083283 2.0449 
220.00092703083283 2.0449 
221.00092703083283 2.0449 
222.00092703083283 2.0449 
223.00092703083283 2.0449 
224.00092703083283 2.0449 
225.00092703083283 2.0449 
226.00092703083283 2.0449 
227.00092703083283 2.0449 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T22:25+0000 #
#####################################################################
