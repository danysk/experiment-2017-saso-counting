#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T13:50+0000 #
#####################################################################
# random = 18.0, targets = 3.1622776601683795, density = 0.05649405772326896, gridShift = 1.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.1731632146381654 NaN 
2.1731632146381656 0.9667215292478952 
3.1731632146381656 0.9635965292478953 
4.173163214638166 0.8819628523676334 
5.173163214638166 0.7124454986071097 
6.173163214638166 0.5283996740944812 
7.173163214638166 0.5070729387184288 
8.173163214638166 0.434814261838167 
9.173163214638166 0.4555927325902717 
10.173163214638166 0.4675444679663241 
11.173163214638166 0.4675444679663241 
12.173163214638166 0.4675444679663241 
13.173163214638166 0.4675444679663241 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T13:50+0000 #
#####################################################################
