#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T00:08+0000 #
#####################################################################
# random = 4.0, targets = 100.0, density = 0.04, gridShift = 0.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0048193674555734 NaN 
2.0048193674555734 1.0356125 
3.0048193674555734 1.0335465277777778 
4.004819367455573 1.0387727864583334 
5.004819367455573 1.016894921875 
6.004819367455573 0.9511383246527778 
7.004819367455573 0.90415625 
8.004819367455573 0.8719 
9.004819367455573 0.8361733072916666 
10.004819367455573 0.8183784288194444 
11.004819367455573 0.8100210937500001 
12.004819367455573 0.8037220052083334 
13.004819367455573 0.7921 
14.004819367455573 0.7921 
15.004819367455573 0.7921 
16.004819367455575 0.7921 
17.004819367455575 0.7921 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T00:08+0000 #
#####################################################################
