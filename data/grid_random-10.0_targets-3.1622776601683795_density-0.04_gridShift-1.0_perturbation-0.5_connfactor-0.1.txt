#####################################################################
# Alchemist log file - simulation started at: 2017-05-22T22:59+0000 #
#####################################################################
# random = 10.0, targets = 3.1622776601683795, density = 0.04, gridShift = 1.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0311320617417066 NaN 
2.0311320617417064 NaN 
3.0311320617417064 1.0 
4.031132061741706 1.0 
5.031132061741706 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-22T22:59+0000 #
#####################################################################
