#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T08:20+0000 #
#####################################################################
# random = 11.0, targets = 10.0, density = 0.07978946395100113, gridShift = 0.0, perturbation = 0.0, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0880725919270569 1.0 
2.088072591927057 1.0 
3.088072591927057 0.99390625 
4.088072591927057 0.9798611111111111 
5.088072591927057 0.9844097222222222 
6.088072591927057 0.9572569444444445 
7.088072591927057 0.9050000000000001 
8.088072591927057 0.7218055555555556 
9.088072591927057 0.5744444444444445 
10.088072591927057 0.51078125 
11.088072591927057 0.6109375 
12.088072591927057 0.64 
13.088072591927057 0.64 
14.088072591927057 0.64 
15.088072591927057 0.64 
16.08807259192706 0.64 
17.08807259192706 0.64 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T08:20+0000 #
#####################################################################
