#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T05:50+0000 #
#####################################################################
# random = 11.0, targets = 1.0, density = 0.05649405772326896, gridShift = 0.0, perturbation = 0.5, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0880725919270569 1.0 
2.088072591927057 1.0 
3.088072591927057 0.953125 
4.088072591927057 0.796875 
5.088072591927057 0.75 
6.088072591927057 0.59375 
7.088072591927057 0.703125 
8.088072591927057 0.765625 
9.088072591927057 0.96875 
10.088072591927057 1.0 
11.088072591927057 1.125 
12.088072591927057 1.234375 
13.088072591927057 0.953125 
14.088072591927057 0.453125 
15.088072591927057 0.5 
16.08807259192706 0.1875 
17.08807259192706 0.0625 
18.08807259192706 0.0625 
19.08807259192706 0.0 
20.08807259192706 0.0 
21.08807259192706 0.0 
22.08807259192706 0.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T05:50+0000 #
#####################################################################
