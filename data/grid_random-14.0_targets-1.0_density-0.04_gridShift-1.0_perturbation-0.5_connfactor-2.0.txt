#####################################################################
# Alchemist log file - simulation started at: 2017-05-24T05:35+0000 #
#####################################################################
# random = 14.0, targets = 1.0, density = 0.04, gridShift = 1.0, perturbation = 0.5, connfactor = 2.0
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.2024264217210618 NaN 
2.2024264217210616 1.0 
3.2024264217210616 1.0648148148148149 
4.202426421721062 1.9907407407407407 
5.202426421721062 4.724537037037037 
6.202426421721062 4.152777777777778 
7.202426421721062 2.814814814814815 
8.20242642172106 1.6689814814814812 
9.20242642172106 0.5069444444444442 
10.20242642172106 0.13194444444444425 
11.20242642172106 0.037037037037036966 
12.20242642172106 1.9721522630525295E-31 
13.20242642172106 1.9721522630525295E-31 
#####################################################################
# End of data export. Simulation finished at: 2017-05-24T05:35+0000 #
#####################################################################
