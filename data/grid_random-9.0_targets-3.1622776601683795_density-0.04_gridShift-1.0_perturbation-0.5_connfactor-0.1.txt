#####################################################################
# Alchemist log file - simulation started at: 2017-05-26T03:02+0000 #
#####################################################################
# random = 9.0, targets = 3.1622776601683795, density = 0.04, gridShift = 1.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.034185494435238 NaN 
2.034185494435238 1.0 
3.034185494435238 0.9556287056638604 
4.034185494435238 0.866886116991581 
5.034185494435238 0.7781435283193017 
6.034185494435238 0.733772233983162 
7.034185494435238 0.6450296453108827 
8.034185494435238 0.6006583509747431 
9.034185494435238 0.5119157623024637 
10.034185494435238 0.5119157623024637 
11.034185494435238 0.4675444679663241 
12.034185494435238 0.4675444679663241 
#####################################################################
# End of data export. Simulation finished at: 2017-05-26T03:02+0000 #
#####################################################################
