#####################################################################
# Alchemist log file - simulation started at: 2017-05-20T12:56+0000 #
#####################################################################
# random = 1.0, targets = 3.1622776601683795, density = 0.3174802103936399, gridShift = 1.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0048855897957667 NaN 
2.0048855897957667 1.0 
3.0048855897957667 1.0 
4.004885589795767 1.0 
5.004885589795767 1.0 
6.004885589795767 1.0 
7.004885589795767 1.0 
8.004885589795766 1.0 
9.004885589795766 1.0 
10.004885589795766 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-20T12:56+0000 #
#####################################################################
