#####################################################################
# Alchemist log file - simulation started at: 2017-05-21T03:11+0000 #
#####################################################################
# random = 2.0, targets = 1.0, density = 0.894427190999916, gridShift = 0.0, perturbation = 0.0, connfactor = 0.5
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.003288451303821 NaN 
2.003288451303821 1.0 
3.003288451303821 1.0 
4.0032884513038205 1.0 
5.0032884513038205 1.0 
6.0032884513038205 1.0 
7.0032884513038205 1.0 
8.00328845130382 1.0 
9.00328845130382 1.0 
10.00328845130382 1.0 
11.00328845130382 1.0 
12.00328845130382 1.0 
13.00328845130382 1.0 
14.00328845130382 1.0 
15.00328845130382 1.0 
16.00328845130382 1.0 
17.00328845130382 1.0 
18.00328845130382 1.0 
19.00328845130382 1.0 
20.00328845130382 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-21T03:11+0000 #
#####################################################################
