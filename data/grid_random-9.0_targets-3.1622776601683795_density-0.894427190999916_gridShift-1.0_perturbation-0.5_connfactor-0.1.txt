#####################################################################
# Alchemist log file - simulation started at: 2017-05-26T03:07+0000 #
#####################################################################
# random = 9.0, targets = 3.1622776601683795, density = 0.894427190999916, gridShift = 1.0, perturbation = 0.5, connfactor = 0.1
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0078258106860618 NaN 
2.007825810686062 1.0 
3.007825810686062 1.0 
4.007825810686062 1.0 
5.007825810686062 1.0 
6.007825810686062 1.0 
7.007825810686062 1.0 
8.007825810686061 1.0 
9.007825810686061 1.0 
10.007825810686061 1.0 
11.007825810686061 1.0 
12.007825810686061 1.0 
13.007825810686061 1.0 
14.007825810686061 1.0 
15.007825810686061 1.0 
16.00782581068606 1.0 
17.00782581068606 1.0 
18.00782581068606 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-26T03:07+0000 #
#####################################################################
