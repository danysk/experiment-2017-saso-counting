#####################################################################
# Alchemist log file - simulation started at: 2017-05-23T06:57+0000 #
#####################################################################
# random = 11.0, targets = 3.1622776601683795, density = 0.4483936332993466, gridShift = 1.0, perturbation = 0.0, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0028758930102715 NaN 
2.0028758930102715 1.0 
3.0028758930102715 1.0 
4.0028758930102715 1.0 
5.0028758930102715 0.9899488430826318 
6.0028758930102715 0.9356482284122234 
7.0028758930102715 0.9482334899411704 
8.002875893010271 0.8948506734068762 
9.002875893010271 0.8672556906357853 
10.002875893010271 0.8022054909727335 
11.002875893010271 0.7757597075551631 
12.002875893010271 0.7097226235425981 
13.002875893010271 0.6201882442309767 
14.002875893010271 0.5634874461980395 
15.002875893010271 0.4717722000753451 
16.00287589301027 0.37792089700430853 
17.00287589301027 0.30853738422875965 
18.00287589301027 0.22906226286962642 
19.00287589301027 0.1626963511806823 
20.00287589301027 0.1488886064164512 
21.00287589301027 0.09998524268311335 
22.00287589301027 0.06268843742840445 
23.00287589301027 0.04903509391590287 
24.00287589301027 0.02830335795521507 
25.00287589301027 0.019442173113884232 
26.00287589301027 0.018284845970551987 
27.00287589301027 0.033911984936771764 
28.00287589301027 0.01962916578893544 
29.00287589301027 0.025651748202359773 
30.00287589301027 0.03193425936160635 
31.00287589301027 0.026951311667395776 
32.00287589301027 0.024831403279910045 
33.00287589301027 0.0406331024442381 
34.00287589301027 0.018544828225757638 
35.00287589301027 0.02899806994657672 
36.00287589301027 0.03243371711464655 
37.00287589301027 0.007287416898036979 
38.00287589301027 0.0026334038989724207 
39.00287589301027 0.0026334038989724207 
40.00287589301027 0.0026334038989724207 
41.00287589301027 0.0026334038989724207 
42.00287589301027 0.0026334038989724207 
43.00287589301027 0.0026334038989724207 
44.00287589301027 0.0026334038989724207 
45.00287589301027 0.0026334038989724207 
46.00287589301027 0.0026334038989724207 
47.00287589301027 0.0026334038989724207 
48.00287589301027 0.0026334038989724207 
49.00287589301027 0.0026334038989724207 
#####################################################################
# End of data export. Simulation finished at: 2017-05-23T06:57+0000 #
#####################################################################
