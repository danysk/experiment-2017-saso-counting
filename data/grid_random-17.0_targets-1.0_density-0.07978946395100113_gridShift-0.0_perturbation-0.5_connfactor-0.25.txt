#####################################################################
# Alchemist log file - simulation started at: 2017-05-25T05:03+0000 #
#####################################################################
# random = 17.0, targets = 1.0, density = 0.07978946395100113, gridShift = 0.0, perturbation = 0.5, connfactor = 0.25
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0045647570182237 NaN 
2.0045647570182235 1.0 
3.0045647570182235 1.0 
4.0045647570182235 1.0 
5.0045647570182235 1.0 
#####################################################################
# End of data export. Simulation finished at: 2017-05-25T05:03+0000 #
#####################################################################
