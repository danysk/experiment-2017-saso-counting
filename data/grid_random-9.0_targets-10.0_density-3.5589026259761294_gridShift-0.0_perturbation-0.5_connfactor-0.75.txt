#####################################################################
# Alchemist log file - simulation started at: 2017-05-26T05:58+0000 #
#####################################################################
# random = 9.0, targets = 10.0, density = 3.5589026259761294, gridShift = 0.0, perturbation = 0.5, connfactor = 0.75
#
# The columns have the following meaning: 
# time se[Mean] 
0.0 NaN 
1.0015323036356916 NaN 
2.001532303635692 NaN 
3.001532303635692 1.0006715277777778 
4.001532303635692 1.0014612847222222 
5.001532303635692 0.9953690972222222 
6.001532303635692 0.9885630208333334 
7.001532303635692 0.9909395833333333 
8.001532303635692 0.9849876736111111 
9.001532303635692 0.9755164930555557 
10.001532303635692 0.9743338541666667 
11.001532303635692 0.9526868055555555 
12.001532303635692 0.9452197916666666 
13.001532303635692 0.9424178819444444 
14.001532303635692 0.9253718750000001 
15.001532303635692 0.9157371527777778 
16.001532303635692 0.8959680555555556 
17.001532303635692 0.8851385416666666 
18.001532303635692 0.8620986111111113 
19.001532303635692 0.8380190972222221 
20.001532303635692 0.8297107638888889 
21.001532303635692 0.8161178819444446 
22.001532303635692 0.7905140624999998 
23.001532303635692 0.7909642361111112 
24.001532303635692 0.773502951388889 
25.001532303635692 0.7452421875 
26.001532303635692 0.7415980902777778 
27.001532303635692 0.7219241319444445 
28.001532303635692 0.7083045138888887 
29.001532303635692 0.6820350694444444 
30.001532303635692 0.6710239583333333 
31.001532303635692 0.6685770833333331 
32.00153230363569 0.6422923611111114 
33.00153230363569 0.6397284722222225 
34.00153230363569 0.6310085069444445 
35.00153230363569 0.5972230902777781 
36.00153230363569 0.5919331597222224 
37.00153230363569 0.5715345486111107 
38.00153230363569 0.5392376736111109 
39.00153230363569 0.5254529513888887 
40.00153230363569 0.5292050347222227 
41.00153230363569 0.497164236111111 
42.00153230363569 0.4879970486111116 
43.00153230363569 0.4773019097222217 
44.00153230363569 0.4475612847222222 
45.00153230363569 0.4511123263888892 
46.00153230363569 0.42467048611111147 
47.00153230363569 0.40353072916666693 
48.00153230363569 0.39757222222222244 
49.00153230363569 0.37944583333333337 
50.00153230363569 0.3379326388888891 
51.00153230363569 0.3426972222222224 
52.00153230363569 0.3174798611111112 
53.00153230363569 0.3015034722222222 
54.00153230363569 0.3084791666666666 
55.00153230363569 0.28326597222222233 
56.00153230363569 0.2806055555555555 
57.00153230363569 0.2697333333333333 
58.00153230363569 0.25220972222222227 
59.00153230363569 0.23198749999999999 
60.00153230363569 0.2224145833333334 
61.00153230363569 0.21884652777777788 
62.00153230363569 0.21355069444444447 
63.00153230363569 0.20138611111111113 
64.00153230363568 0.20229861111111114 
65.00153230363568 0.18632152777777794 
66.00153230363568 0.31952638888888885 
67.00153230363568 0.5887895833333328 
68.00153230363568 0.4353215277777774 
69.00153230363568 1.4305159722222272 
70.00153230363568 1.8498473958333312 
71.00153230363568 1.7004895833333318 
72.00153230363568 3.1302774305555543 
73.00153230363568 3.120603125 
74.00153230363568 1.8401241319444437 
75.00153230363568 3.7326680555555543 
76.00153230363568 2.848931944444442 
77.00153230363568 2.3917043402777813 
78.00153230363568 4.447826736111106 
79.00153230363568 3.28041701388889 
80.00153230363568 4.024147222222218 
81.00153230363568 4.1574512152777725 
82.00153230363568 3.856638715277776 
83.00153230363568 2.640345312499998 
84.00153230363568 2.9596914930555496 
85.00153230363568 2.7485041666666663 
86.00153230363568 2.5558229166666604 
87.00153230363568 2.270600347222217 
88.00153230363568 2.7688996527777747 
89.00153230363568 1.921576736111113 
90.00153230363568 1.8968626736111014 
91.00153230363568 1.1465666666666647 
92.00153230363568 0.7665380208333328 
93.00153230363568 0.45947204861111246 
94.00153230363568 0.2623687500000009 
95.00153230363568 0.24600312499999993 
96.00153230363568 0.29557968750000047 
97.00153230363568 0.1902656250000002 
98.00153230363568 0.18825625000000049 
99.00153230363568 0.1166453124999994 
100.00153230363568 0.0630750000000003 
101.00153230363568 0.027531249999999823 
102.00153230363568 0.01 
103.00153230363568 0.01 
104.00153230363568 0.01 
105.00153230363568 0.01 
106.00153230363568 0.01 
107.00153230363568 0.01 
108.00153230363568 0.01 
109.00153230363568 0.01 
110.00153230363568 0.01 
111.00153230363568 0.01 
112.00153230363568 0.01 
113.00153230363568 0.01 
114.00153230363568 0.01 
115.00153230363568 0.01 
116.00153230363568 0.01 
117.00153230363568 0.01 
118.00153230363568 0.01 
119.00153230363568 0.01 
120.00153230363568 0.01 
121.00153230363568 0.01 
122.00153230363568 0.01 
123.00153230363568 0.01 
124.00153230363568 0.01 
125.00153230363568 0.01 
126.00153230363568 0.01 
127.00153230363568 0.01 
128.00153230363568 0.01 
129.00153230363568 0.01 
130.00153230363568 0.01 
131.00153230363568 0.01 
132.00153230363568 0.01 
133.00153230363568 0.01 
134.00153230363568 0.01 
135.00153230363568 0.01 
136.00153230363568 0.01 
137.00153230363568 0.01 
138.00153230363568 0.01 
139.00153230363568 0.01 
140.00153230363568 0.01 
141.00153230363568 0.01 
142.00153230363568 0.01 
143.00153230363568 0.01 
144.00153230363568 0.01 
#####################################################################
# End of data export. Simulation finished at: 2017-05-26T06:00+0000 #
#####################################################################
