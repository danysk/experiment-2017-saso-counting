import com.google.common.cache.CacheBuilder
import com.google.common.cache.LoadingCache
import com.google.common.collect.Sets
import it.unibo.alchemist.model.interfaces.Node
import java.util.Collections
import java.util.Map
import java.util.concurrent.TimeUnit
import org.protelis.lang.datatype.DatatypeFactory
import org.protelis.lang.datatype.Tuple
import gnu.trove.map.TIntObjectMap
import gnu.trove.list.TIntList
import gnu.trove.list.array.TIntArrayList
import gnu.trove.map.hash.TIntObjectHashMap

class CountingUtil {
	
	private static Tuple EMPTY_TUPLE = DatatypeFactory.createTuple()
	private static val LoadingCache<TIntObjectMap<TIntList>, Double> CACHE = CacheBuilder
	.newBuilder
	.maximumSize(1000)
	.expireAfterAccess(1, TimeUnit.MINUTES)
//	.recordStats
	.build[chifast]
	private new() {}
	
	def static Map<Object, Object> asMap(Tuple t) {
		t.groupBy[
			if (it instanceof Tuple) {
				if (it.size != 2) {
					throw new IllegalArgumentException("Not a tuple of 2-ples")
				}
				it.get(0)
			} else {
				throw new IllegalArgumentException("Not a tuple of tuples")
			}
		]
		.mapValues[it.get(0) as Tuple]
		.mapValues[it.get(1)]
	}
	
	def static Tuple range(int max) {
		range(0, max)
	}

	def static Tuple range(int min, int max) {
		DatatypeFactory.createTuple(
			if (min <= max) {
				(min .. max).toList
			} else {
				Collections.emptyList
			}
		)
	}
	
	def static double chi(Map<Integer, Tuple> neighborsNeighs) {
		val intermediate1 = Sets.powerSet(neighborsNeighs.keySet)
			.groupBy[it.size]
			.mapValues[
				it.filter[set | set.forall[device |
//					neighborsNeighs.getOrDefault(device, EMPTY_TUPLE)
//						.containsAll(set.reject[it == device])
					val mustContain = set.reject[it.equals(device)]
					val targetTuple = neighborsNeighs.getOrDefault(device, EMPTY_TUPLE)
					mustContain.forall[targetTuple.contains(it)]
//					val mustContain = set.reject[it.equals(device)]
//					neighborsNeighs.getOrDefault(device, EMPTY_TUPLE)
//						.toArray
//						.containsAll(mustContain.toSet)
				]]
			]
		val intermediate2 = intermediate1.mapValues[it.size as double]
			.entrySet
		val intermediate3 = intermediate2.map[(if (it.key % 2 == 0) 1 else -1) * it.value / (it.key + 1)]
		intermediate3
			.reduce[$0 + $1]
	}

	def static double chifast(TIntObjectMap<TIntList> neighborsNeighs) {
		Sets.powerSet(neighborsNeighs.keys.toSet)
			.groupBy[it.size]
			.mapValues[
				it.filter[set | set.forall[device |
					(neighborsNeighs.get(device) ?: new TIntArrayList(0))
						.containsAll(set.reject[it == device])
				]]
			]
			.mapValues[it.size as double]
			.entrySet
			.map[(if (it.key % 2 == 0) 1 else -1) * it.value / (it.key + 1)]
			.reduce[$0 + $1]
	}

	def static double cachedChi(int h, Node<?> myself, Map<Integer, Tuple> neighborsNeighs) {
//		println(CACHE.stats.hitRate * 100)
		val fastCopy = new TIntObjectHashMap(neighborsNeighs.size, 1f, -1)
		val nodeIds = new TIntArrayList()
		for (e: neighborsNeighs.entrySet) {
			if (!nodeIds.contains(e.key)) {
				nodeIds.add(e.key)
			}
			for (tv: e.value) {
				if (!nodeIds.contains(tv as Integer)) {
					nodeIds.add(tv as Integer)
				}
			}
		}
		nodeIds.sort
		neighborsNeighs.forEach[
			fastCopy.put(nodeIds.indexOf($0), new TIntArrayList($1.toArray.map[nodeIds.indexOf(it as Integer)]))
		]
		CACHE.get(fastCopy)
	}
	
	def static void main(String... a) {
		println(Sets.powerSet((#{0, 1, 2})).toList)
	}
}