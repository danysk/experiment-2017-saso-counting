package it.unibo.alchemist.model.implementations.linkingrules

import it.unibo.alchemist.model.interfaces.Node
import it.unibo.alchemist.model.interfaces.Environment
import it.unibo.alchemist.model.interfaces.Molecule
import it.unibo.alchemist.model.implementations.neighborhoods.CachedNeighborhood
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * This class models a LinkingRule that links using EuclideanDistance, reducing the range in case a molecule is present in one of the a potential neighbors.
 */
@Accessors(PUBLIC_GETTER, PROTECTED_SETTER)
class MoleculeBasedRange<T> extends EuclideanDistance<T> {

	val Molecule mol;
	val double otherRange
	val boolean otherLower

	new(double range, Molecule mol, double otherRange){
		super(Math.max(range, otherRange))
		this.mol = mol
		this.otherRange = Math.min(range, otherRange)
		otherLower = otherRange < range
	}

	override computeNeighborhood(Node<T> center, Environment<T> env) {
		val centerHasMol = center.contains(mol)
		val bothContainMolecule = [Node<T> n | centerHasMol || n.contains(mol)]
		val neighborhood = super.computeNeighborhood(center, env).reject[
			!otherLower.xor(bothContainMolecule.apply(it)) && env.getDistanceBetweenNodes(center, it) > otherRange
		].toList
		new CachedNeighborhood(center, neighborhood, env)
	}
	
}