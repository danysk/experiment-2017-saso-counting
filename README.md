# Self-stabilising target counting in wireless sensor networks using Euler integration

Submitted for publication in the [11th IEEE International Conference on Self-Adaptive and Self-Organizing Systems](https://saso2017.telecom-paristech.fr/). For any issues with reproducing the experiments, please contact [Danilo Pianini](mailto:danilo.pianini@unibo.it).

## Description

This repository contains all the source code used to perform the experiments presented in the paper.

## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 8 JDK update 112 or above installed and set as default virtual machine
    - The javac and java commands must be available from the terminal

* Chrunching the generated data and producing the charts
    - Python 3.6.1 or above installed and set as default Python interpreter
    - Matplotlib 2.0.2 or above
    - NumPy 1.12.1 or above
  
**Notes about the supported operating systems**

All the tests have been performed under Linux, no test has been performed under any other operating system, and we only support reproducing the tests under that environment.
Due to the portable nature of the used tools, the test suite might run also on other Unix-like OSs (e.g. BSD, MacOS X), and possibly under Microsoft Windows, provided that the prerequisites are met.
The detection of the available installed memory used to fine tune the simulation batch performance requires read access to `/proc/meminfo`, and it is only available under Linux.
All the commands that follow in this README file are meant for being used on Linux, there is a chance they will work on other Unix-like OSs, they are not going to work under Microsoft Windows.

## Reproducing the experiment

### Importing the repository

The first step is cloning this repository.
It can be easily done by using `git`.
Open a terminal, move it into an empty folder of your choice, then issue the following command:

``git clone git@bitbucket.org:danysk/experiment-2017-saso-counting.git .``

In case you have not set up your ssh keys correctly, you can work the problem around by copying via HTTPS.

This should make all the required files appear in your repository at their latest version.

### Re-executing the simulations

The whole set of simulations can be executed by pointing the terminal in the root directory where the repository was cloned, and issuing:

``./gradlew rungrid``

Results will be written in the `data` directory.
Please make sure that such directory exists.
Older files in such folder whose name matches the data export name will be overwritten without prior notice.

*Note*
Windows users may try to use: `gradlew.bat`. It should produce the same results of `./gradlew`, but it has not been tested and it's here for testing purposes only.

### Crunching data

For those who are not interested in re-running the whole experiment, but want to fiddle with the generated data, we are tracking all the raw data generated by our execution.
There is a Python 3 script that processes such raw data out and computes the average over the executed runs.
It can be launched by pointing a terminal in the repository root folder and issuing:

``python preprocess.py``

Such script will save the summarized data within the same `data` folder used for the raw data

### Producing the graphs

A Python 3 script is available for producing the charts.
It runs over the data produced by the previous step (which is tracked in this repository, for those who only want to regenerate the charts), and produces over 500 charts, with all the combinations of diverse proposed building blocks.
It can be executed from a terminal pointing to the root folder of this repository by issuing:

``python makecharts.py``

## Tested platforms

We successfully ran the experiments with the following system:

  * Intel(R) Xeon(R) CPU           X7350  @ 2.93GHz
  * 16GB RAM
  * Linux 4.8.13-1-ARCH #1 SMP PREEMPT Fri Dec 9 07:24:34 CET 2016 x86_64 GNU/Linux
  * OpenJDK 64-Bit Server VM (build 25.112-b15, mixed mode)
  * javac 1.8.0_121
