## Test Plan

### How does distribution affect the counting?
We run tests with different coverages:
 * regular grid
 * perturbed grid
 * uniform random distribution
 * geometrically varying distribution
 * higher density spots
We could also run tests with progressive perturbations of a grid, and se how the counting gets progressively disrupted, providing an error/randomness chart.

### How does density affect the counting?
We run tests with varying densities. We could have a chart with
error/density, with one line per spatial distribution.

### How do the counting get disrupted by devices on the edge?
We could run experiments with a regular grid of devices and targets
randomly spread outside the grid but within the sensing radius of the
sensors. We can chart error/number of targets. I expect the error to
converge to the sensed area minus the area covered by two sensors.

### How does target mobility affect the counting?
We measure this by deploying a slightly perturbed grid of sensors
in a simulated corridor and letting targets walk through it at varying
speed. We then produce a heatmap with error/speed/target count.

### How does sensor mobility affect the counting?
We measure this by deploying random static targets within an arena,
and we let sensor-robots explore it moving around at varying speed. We
then create a heatmap with error/speed/sensor count.

### Which results would the system provide on a real-world deployment?
We try our approach on the VCM 2013 dataset, by deploying a static
network of sensors along the streets and enabling the counting.

### How can we use the counting other than just counting target?
We can use it to better compute averages in case of non-uniform
device distributions in space. To prove it, we can deploy a network
with some dense spots, and have it sense some continuous field (it
could be temperature, pollution, electrosmog, or radioactivity - it
would be extremely nice to have real world data here!) of which we can
compute the real average, and show that we can average it correctly.

### Configuration and charts

simulation files:

  * grid [devices, targets, perturbation]
  * random [devices, targets]
  * geometric [devices, targets]
  * spots [devices, targets]
  * outside - targets are theoretically reachable by some sensors, but are outside the main grid [devices, targets]
  * moving targets run through a random sensors [speed]
  * moving sensors run through random targets [speed]
  * vienna [devices]
  * vienna-onlypeople
  * integral - spots + layer with 2d-gaussian [devices]
  
common variables:
  * random ~10 samples
  * perturbation 0 everywhere, 0.5 in perturbed grid
  * density (devices/m2) (geometric in [1, 1000], 19 samples)
  * "targets" (geometric in [1, 10000], 9 samples)
  * speed: 0 everywhere, 0.1 to 10 m/s in speed
  
exports:

  * time
  * squared error (targets is reference value)
  
simulations:

  * devices * targets * random * speed
  
charts:

  * error / devices / targets (heatmap) * [all -outside] - if targets are not relevant, one multiline chart
  * error / targets / devices (heatmap or multiline chart) * [outside]
  * error / speed * [moving targets + moving devices]
  * error / time (multiline: vienna devices and onlypeople)

