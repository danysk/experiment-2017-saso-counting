import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import matplotlib.lines as mlines
import fnmatch
import os
import math
import re
from itertools import combinations
from itertools import product
from mpl_toolkits.mplot3d import Axes3D


def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    if match is None:
        return math.nan
    return float(match.group(0))

def lastRes(file):
    data = np.load(directory + '/' + file)
    return data[-1, 1]

# CONFIGURE SCRIPT
np.set_printoptions(formatter={'float': '{: 0.6f}'.format})
titlesize = 12
axissize = 10
legendsize = 9
directory = 'data'
variables = {}
xvar = 'density'
yvar = 'targets'
figure_size=(6, 3)
for varname in [xvar, yvar, 'perturbation', 'gridShift', 'connfactor']:
    varVals = {getVarValue(varname, target) for target in os.listdir(directory)}
    variables[varname] = set(filter(lambda x: not np.isnan(x), varVals))
itervals = {k:v for (k, v) in variables.items() if xvar not in k and yvar not in k}
valsets = list(set(map(lambda x: (name, x), itervals[name])) for name in sorted(itervals))
for graphvals in product(*valsets):
    files = [x for x in os.listdir(directory) if 'mean' in x and 'npy' in x]
    errfiles = [x for x in os.listdir(directory) if 'err' in x and 'npy' in x]
    for varval in graphvals:
        mustcontain = varval[0]+'-' + str(varval[1])
        files = [x for x in files if mustcontain in x]
        errfiles = [x for x in errfiles if mustcontain in x]

    # 3D Plot
#    fig = plt.figure(figsize=figure_size)
#    datapoints = {(getVarValue(xvar, file), getVarValue(yvar, file)) : lastRes(file) for file in files}
#    xvals = sorted({getVarValue(xvar, file) for file in files}, reverse=True)
#    yvals = sorted({getVarValue(yvar, file) for file in files})
#    x, y = np.meshgrid(xvals, yvals)
#    miter = np.nditer(x, flags=['multi_index'])
#    z = x.copy()
#    while not miter.finished:
#        i = miter.multi_index
#        z[i] = datapoints[(x[i], y[i])]
#        miter.iternext()
#    ax = fig.add_subplot(111, projection='3d')
#    ax.invert_xaxis()
#    ax.plot_surface(np.log(x), np.log(y), z)

#     Multiline plot
    fig = plt.figure(figsize=figure_size)
    ax = fig.add_subplot(111)
    ax.set_ylabel("Error (normalized RMSE)")
    ax.set_xlabel("Device density (devices/m²)")
    ax.set_xscale('log')
    graphvals = dict(graphvals)
    title = {0.0: 'Regular ', 0.5: 'Perturbed '}[graphvals['perturbation']]
    title += { 0.0: 'square ', 1.0: 'hexagonal '}[graphvals['gridShift']]
    scr = graphvals['connfactor']
    title += 'grid, SCR ' + str(scr)
    ax.set_title(title)
    datapoints = {getVarValue(yvar, file) for file in files}
    top = 1
    for targetCount in sorted(datapoints):
        data = [[getVarValue(xvar, file), lastRes(file)] for file in files if getVarValue(yvar, file) == targetCount]
        data = sorted(data)
        data = np.matrix(data)
        errdata = [[getVarValue(xvar, file), lastRes(file)] for file in errfiles if getVarValue(yvar, file) == targetCount]
        errdata = sorted(errdata)
        errdata = np.matrix(errdata)
        ax.errorbar(data[:, 0], data[:, 1], yerr=errdata[:, 1], barsabove=True, label = "{:1.0f}".format(targetCount), capsize=0)
        top = max(top, max(data[:, 1] + errdata[:, 1]))
    ax.set_ylim(0, top*1.05)
    ax.set_xlim(min(data[:, 0])*0.95, max(data[:, 0])*1.05)
    ax.legend(fontsize = legendsize, loc='best', ncol=5)
    ax.plot((0.04, 20), (1, 1), 'k--')
    fig.tight_layout()
    fig.savefig('Density ' + title.replace('=',' ').replace('.', '_').replace(',', '') + '.pdf')

# Connection Factor charts
allfiles = [x for x in os.listdir(directory) if 'npy' in x]
for density in variables[xvar]:
    densityStr = "{:1.2f}devices/m²".format(density)
    for targets in variables[yvar]:
        targetsStr = "{:1.0f}".format(targets) + ' targets'
        fig = plt.figure(figsize=figure_size)
        ax = fig.add_subplot(111)
        ax.set_ylabel("Error(Normalized RMSE)")
        ax.set_xlabel("Sensing/communication distance ratio (SCR)")
        title = "Error with SCR, " + densityStr + ', ' + targetsStr
        ax.set_title(title)
#        ax.set_yscale('log')
        top = 1
        for perturbation in variables['perturbation']:
            labelroot = {0.0: 'Reg. ', 0.5: 'Per. '}[perturbation]
            for shift in variables['gridShift']:
                label = labelroot + { 0.0: 'sq.', 1.0: 'hex.'}[shift]
                def valid(stat, myfile):
                    def has(what):
                        return what in myfile
                    return (has(stat) and has('npy')
                            and has(xvar+'-'+str(density))
                            and has(yvar+'-'+str(targets))
                            and has('perturbation-'+str(perturbation))
                            and has('gridShift-'+str(shift)))
                files = [x for x in allfiles if valid('mean', x)]
                errfiles = [x for x in allfiles if valid('err', x)]
                data = np.matrix(sorted([[getVarValue('connfactor', file), lastRes(file)] for file in files]))
                err = np.matrix(sorted([[getVarValue('connfactor', file), lastRes(file)] for file in errfiles]))
                for i, (d, e) in enumerate(zip(data[:, 1], err[:, 1])):
                    if d > 1:
                        err[i, 1] = 0
                ax.errorbar(data[:, 0], data[:, 1], yerr=err[:, 1], barsabove=True, label = label, capsize=0)
                top = max(top, max(data[:, 1] + err[:, 1]))
        ax.legend(fontsize = legendsize, loc='best', ncol=1)
        ax.set_ylim(0, 1.05)
        fig.tight_layout()
        fig.savefig('SCR ' + title.replace('=',' ').replace('.', '_').replace(',', '').replace('/', '').replace('²', '') + '.pdf')
                
# Stabilization times chart
targets = "31.622776601683793"
perturbations = ["0.0", "0.5"]
displacements = ["0.0", "1.0"]
densities = variables['density']
scr = "0.75"

fig = plt.figure(figsize=figure_size)
ax = fig.add_subplot(111)
ax.set_ylabel("Error(Normalized RMSE)")
ax.set_xlabel("Sensing/communication distance ratio (SCR)")
#ax.set_xlim(0, 450)
ax.set_xscale('log')
for shape in displacements:
    for perturbation in perturbations:
        data = []
        for density in sorted(densities):
            file = [f for f in os.listdir(directory) if 'npy' in f
                 and targets in f
                 and perturbation in f
                 and scr in f
                 and shape in f
                 and 'mean' in f
                 and str(density) in f]
            filecnt = np.load(directory + '/' + file[0])
            searchElem = filecnt[-1, 1]
            time = next(x[0] for x in np.flip(filecnt, 0) if x[1] != searchElem)
            if len(data) == 0:
                data = np.matrix([[density, time]])
            else:
                data = np.vstack((data, [density, time]))
    #        density = float(density)
    #        label = { "0.0": 'sq.', "1.0": 'hex.'}[shape] + " grid, {:1.2f}d/m²".format(int(density) if density.is_integer() else density)
    #        label = "{:1.2f}d/m²".format(int(density) if density.is_integer() else density)
            label = {"0.0": 'Reg. ', "0.5": 'Per. '}[perturbation] + { "0.0": 'sq.', "1.0": 'hex.'}[shape] + ' grid'
        ax.plot(data[:,0], data[:,1], label=label)
ax.legend(fontsize = legendsize, loc='best', ncol=1)

#allfiles = [x for x in os.listdir(directory) if 'npy' in x]
#for density in variables[xvar]:
#    densityStr = "{:1.2f}devices/m²".format(density)
#    for targets in variables[yvar]:
#        targetsStr = "{:1.0f}".format(targets) + ' targets'
#        fig = plt.figure(figsize=figure_size)
#        ax = fig.add_subplot(111)
#        ax.set_ylabel("Error(Normalized RMSE)")
#        ax.set_xlabel("Sensing/communication distance ratio (SCR)")
#        title = "Error with SCR, " + densityStr + ', ' + targetsStr
#        ax.set_title(title)
##        ax.set_yscale('log')
#        top = 1
#        for perturbation in variables['perturbation']:
#            labelroot = {0.0: 'Reg. ', 0.5: 'Per. '}[perturbation]
#            for shift in variables['gridShift']:
#                label = labelroot + { 0.0: 'sq.', 1.0: 'hex.'}[shift]
#                def valid(stat, myfile):
#                    def has(what):
#                        return what in myfile
#                    return (has(stat) and has('npy')
#                            and has(xvar+'-'+str(density))
#                            and has(yvar+'-'+str(targets))
#                            and has('perturbation-'+str(perturbation))
#                            and has('gridShift-'+str(shift)))
#                files = [x for x in allfiles if valid('mean', x)]
#                errfiles = [x for x in allfiles if valid('err', x)]
#                data = np.matrix(sorted([[getVarValue('connfactor', file), lastRes(file)] for file in files]))
#                err = np.matrix(sorted([[getVarValue('connfactor', file), lastRes(file)] for file in errfiles]))
#                for i, (d, e) in enumerate(zip(data[:, 1], err[:, 1])):
#                    if d > 1:
#                        err[i, 1] = 0
#                ax.errorbar(data[:, 0], data[:, 1], yerr=err[:, 1], barsabove=True, label = label, capsize=0)
#                top = max(top, max(data[:, 1] + err[:, 1]))
#        ax.legend(fontsize = legendsize, loc='best', ncol=1)
#        ax.set_ylim(0, 1.05)
#        fig.tight_layout()
#        fig.savefig('SCR ' + title.replace('=',' ').replace('.', '_').replace(',', '').replace('/', '').replace('²', '') + '.pdf')
                


#titles = {'wsn': 'Channel pattern', 'london': 'Context sensitive gradient'}
#ylabels = {'wsn': 'P(channel)', 'london': 'Mean gradient value (m)'}
#xlabel_aggregate = 'Devices'
#xlabel = 'Time (simulated seconds)'
#titlesize = 12
#axissize = 10
#legendsize = 9
#colormap = plt.get_cmap('jet')
#linewidth = 2
#figure_size=(6, 3)
#
#def unstableFor(points, maxStable):
#    count = 0;
#    prev = points[0]
#    for i in range(1, len(points)):
#        if points[i] == prev:
#            count = count + 1
#        else:
#            count = 0
#            prev = points[i]
#        if count >= maxStable:
#            return i
#    return len(points) - 1
#
#
#for experiment in experiments:
#    files = experiment + '-mean*.npy'
#    for file in [directory + '/' + f for f in os.listdir(directory) if fnmatch.fnmatch(f, files)]:
#        # G error
#        istime = bool(float(getVarValue('time', file)))
#        issmall = bool(float(getVarValue('small', file)))
#        data = np.load(file)
#        # G/Time
#        fig_gtime = plt.figure(figsize=figure_size)
#        gtime_ax = fig_gtime.add_subplot(1, 1, 1)
#        title = 'small' if issmall else 'large'
#        title += ' perturbation in '
#        title += 'time' if istime else 'space'
#        gtime_title = 'G: ' + title
#        gtime_ax.set_title(gtime_title, fontsize=titlesize)
#        gtime_ax.set_xlabel('Time (s)', fontsize=axissize)
#        gtime_ax.set_ylabel('Mean error (m)', fontsize=axissize)
##        gtime_ax.grid(b = True, which = 'both')
#        gtime_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#        gtime_ax.set_yscale('log')
#        maxy = 0
#        time = data[:, 0]
#        maxindex = 0
#        filterout = 6
#        for name, index in {
#            'G' : 3,
##            'CRF-2': 9,
#            'CRF': 6,
#            'FLEX': 12
#        }.items():
#            points = data[:, index]
#            gtime_ax.plot(time, points, label=name)
#            maxy = max(np.nanmax(points[filterout:]), maxy)
#            maxindex = max(maxindex, unstableFor(points, 10))
#        gtime_ax.legend(fontsize = legendsize, loc='best', ncol=4)
#        gtime_ax.set_xlim(min(time), time[maxindex])
#        gtime_ax.set_ylim(0, maxy * 2)
#        fig_gtime.savefig(gtime_title.replace(':', '')+'.pdf')
#        
##        G stability
#        fig_gstab = plt.figure(figsize=figure_size)
#        gstab_ax = fig_gstab.add_subplot(1, 1, 1)
#        gstab_ax.set_title(gtime_title, fontsize=titlesize)
#        gstab_ax.set_xlabel('Time (s)', fontsize=axissize)
#        gstab_ax.set_ylabel('Mean value change (m)', fontsize=axissize)
##        gstab_ax.grid(b = True, which = 'both')
#        gstab_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#        maxy = 0
#        miny = math.inf
#        time = data[:, 0]
#        maxindex = 0
#        filterout = 10
#        for name, index in {
#            'G' : 4,
##            'CRF-2': 10,
#            'CRF': 7,
#            'FLEX': 13
#        }.items():
#            points = data[:, index]
##            gstab_ax.plot(time, points, '.', ms=3, label=name)
#            gstab_ax.plot(time, points, label=name)
#            maxy = max(np.nanmax(points[filterout:]), maxy)
#            miny = min(np.nanmin(np.trim_zeros(points[filterout:])), miny)
#            maxindex = max(maxindex, unstableFor(points, 10))
#        gstab_ax.legend(fontsize = legendsize, loc='best', ncol=3)
#        gstab_ax.set_xlim(min(time), time[maxindex])
#        gstab_ax.set_ylim(miny, maxy * 1.2)
#        if not istime:
#            gstab_ax.set_yscale('log')
#        fig_gstab.savefig(gtime_title.replace(':', '')+'-stability.pdf')
#        
#        # C
#        fig_c = plt.figure(figsize=figure_size)
#        c_ax = fig_c.add_subplot(1, 1, 1)
#        c_title = 'C: ' + title
#        c_ax.set_title(c_title, fontsize=titlesize)
#        c_ax.set_xlabel('Time (s)', fontsize=axissize)
#        c_ax.set_ylabel('Estimated device count', fontsize=axissize)
##        c_ax.grid(b = True, which = 'both')
#        c_ax.tick_params(axis='both', which='both', labelsize=int(axissize*0.8))
#        maxy = 0
#        miny = math.inf
#        time = data[:, 0]
#        maxindex = 0
#        filterout = 20
#        for name, index in {
#            'C' : 15,
#            'C-multipath': 16,
#            'Expected': 14,
#        }.items():
#            points = data[:, index]
#            c_ax.plot(time, points, label=name)
#            maxy = max(np.nanmax(points[filterout:]), maxy)
#            miny = min(np.nanmin(np.trim_zeros(points[filterout:])), miny)
#            maxindex = max(maxindex, unstableFor(points, 10))
#        c_ax.legend(fontsize = legendsize, loc='best', ncol=1)
#        c_ax.set_xlim(min(time), time[maxindex])
#        c_ax.set_ylim(max(1, miny)*0.9, maxy*1.1)
#        c_ax.set_yscale('log')
#        fig_c.savefig(c_title.replace(':', '')+'.pdf')
#
#        fig_cerr = plt.figure(figsize=figure_size)
#        cerr_ax = fig_cerr.add_subplot(1, 1, 1)
#        cerr_title = 'C: ' + title
#        cerr_ax.set_title(cerr_title, fontsize=titlesize)
#        cerr_ax.set_xlabel('Time (s)', fontsize=axissize)
#        cerr_ax.set_ylabel('Error (device count)', fontsize=axissize)
##        cerr_ax.grid(b = True, which = 'both')
#        cerr_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#        maxy = 0
#        miny = math.inf
#        time = data[:, 0]
#        maxindex = 0
#        filterout = 10
#        for name, index in {
#            'C' : 15,
#            'C-multipath': 16
#        }.items():
#            points = data[:, index] - data[:, 14]
#            cerr_ax.plot(time, np.absolute(points), label=name)
#            maxy = max(np.nanmax(points[filterout:]), maxy)
#            miny = min(np.nanmin(np.trim_zeros(points[filterout:])), miny)
#            maxindex = max(maxindex, unstableFor(points, 10))
#        cerr_ax.legend(fontsize = legendsize, loc='best', ncol=2)
#        cerr_ax.set_xlim(min(time), time[maxindex])
#        cerr_ax.set_ylim(max(0, miny), maxy)
##        if istime:
##            cerr_ax.set_ylim(max(5, miny), maxy)
##            cerr_ax.set_yscale('log')
#        fig_cerr.savefig(cerr_title.replace(':', '')+'-err.pdf')
#        
#        #T Time
#        fig_ttimesin = plt.figure(figsize=figure_size)
#        ttimesin_ax = fig_ttimesin.add_subplot(1, 1, 1)
#        ttimesin_title = 'T: ' + title + ', sine wave driver'
#        ttimesin_ax.set_title(ttimesin_title, fontsize=titlesize)
#        ttimesin_ax.set_xlabel('Time (s)', fontsize=axissize)
#        ttimesin_ax.set_ylabel('Root mean squared error', fontsize=axissize)
##        ttimesin_ax.grid(b = True, which = 'both')
#        ttimesin_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#        maxy = -math.inf
#        miny = math.inf
#        time = data[:, 0]
#        for name, curline in {
##            'Ideal' : data[:, 17],
#            'T' : data[:, 25],
##            'Tsync, a=0.01' : data[:, 29],
#            'T\', a=0.02' : data[:, 33],
##            'Tsync, a=0.05' : data[:, 37],
##            'Tsync, a=0.1' : data[:, 41],
##            'Tsync, a=0.2' : data[:, 45],
#            'T\', a=0.5' : data[:, 49],
##            'Max' : data[:, 19],
##            'Min' : data[:, 20],
##            'Noise' : data[:, 21],
##            'Sinnoise' : data[:, 22],
#        }.items():
#            points = np.sqrt(curline)
#            ttimesin_ax.plot(time, points, label=name)
#            maxy = max(np.nanmax(points[30:]), maxy)
#            miny = min(np.nanmin(np.trim_zeros(points[30:])), miny)
#        ttimesin_ax.legend(fontsize = legendsize, loc='best', ncol=3)
#        ttimesin_ax.set_xlim(min(time), max(time))
#        ttimesin_ax.set_ylim(miny, maxy * 1.2)
#        ttimesin_ax_2 = ttimesin_ax.twinx()
#        ttimesin_ax_2.plot(time, data[:, 17], color='black', label='Driver')
#        ttimesin_ax_2.set_xlim(min(time), max(time))
#        ttimesin_ax_2.set_ylim(-1.4, 1.4)
#        ttimesin_ax.set_zorder(ttimesin_ax_2.get_zorder() + 1)
#        ttimesin_ax.patch.set_visible(False)
#        fig_ttimesin.savefig(ttimesin_title.replace(':', '')+'.pdf')
#        
#        #T Time
#        fig_ttimerect = plt.figure(figsize=figure_size)
#        ttimerect_ax = fig_ttimerect.add_subplot(1, 1, 1)
#        ttimerect_title = 'T: ' + title + ', square wave driver'
#        ttimerect_ax.set_title(ttimerect_title, fontsize=titlesize)
#        ttimerect_ax.set_xlabel('Time (s)', fontsize=axissize)
#        ttimerect_ax.set_ylabel('Mean squared error', fontsize=axissize)
##        ttimerect_ax.grid(b = True, which = 'both')
#        ttimerect_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#        maxy = -math.inf
#        miny = math.inf
#        time = data[:, 0]
#        for name, curline in {
#            'T' : data[:, 27],
##            'Tsync, a=0.01' : data[:, 31],
#            'T\', a=0.02' : data[:, 35],
##            'Tsync, a=0.05' : data[:, 39],
##            'Tsync, a=0.1' : data[:, 43],
##            'Tsync, a=0.2' : data[:, 47],
#            'T\', a=0.5' : data[:, 51],
#        }.items():
#            points = np.sqrt(curline)
#            ttimerect_ax.plot(time, points, label=name)
#            maxy = max(np.nanmax(points[30:]), maxy)
#            miny = min(np.nanmin(np.trim_zeros(points[30:])), miny)
#        ttimerect_ax.legend(fontsize = legendsize, loc='best', ncol=3)
#        ttimerect_ax.set_xlim(min(time), max(time))
#        ttimerect_ax.set_ylim(miny, maxy * 1.2)
#        ttimerect_ax_2 = ttimerect_ax.twinx()
#        ttimerect_ax_2.plot(time, data[:, 18], color='black', label='Driver')
#        ttimerect_ax_2.set_xlim(min(time), max(time))
#        ttimerect_ax_2.set_ylim(-0.4, 1.4)
##        ttimerect_ax.set_zorder(ttimerect_ax_2.get_zorder() + 1)
##        ttimerect_ax.patch.set_visible(False)
#        fig_ttimerect.savefig(ttimerect_title.replace(':', '')+'.pdf')
#
#
#
#experiments = {
#    'crowdsize': {
#        'title': 'Crowd size estimation',
#        'ylabel': 'Average error (counted devices)',
#    },
#    'alert': {
#        'title': 'Evacuation alert',
#        'ylabel': 'Mean square error (radians)',
#    }
#}
#indices = {
#    "GCT": 1,
#    "GCS": 2,
#    "GMT": 3,
#    "GMS": 4,
#    "FCT": 5,
#    "FCS": 6,
#    "FMT": 7,
#    "FMS": 8,
#}
#colors = ['r', 'y', 'b', 'grey', 'g', 'm', 'c', 'k']
#def idxfor(expname):
#    return [idx for name, idx in indices.items() if expname in name]
#
#for experiment, expdata in experiments.items():
#    titlebase = expdata['title']
#    ylabel = expdata['ylabel']
#    data = {}
#    i = 0
#    files = experiment + '*map-mean*_.txt.npy'
#    for file in [directory + '/' + f for f in os.listdir(directory) if fnmatch.fnmatch(f, files)]:
#        print(file)
#        data[i] = np.load(file)
#        i = i+1
#    maxfdata = data[max(data)]
#    time = maxfdata[:, 0]
#    maxy = 0
#    minindex = min(indices.values())
#    maxindex = max(indices.values())
#    
#    
#    for length in range(2, maxindex + 1):
#        for combination in [n for n in combinations(indices, length)]:# if 'G-C-T' in n and 'F-M-S' in n]:
#            # Time charts
#            fig_time = plt.figure(figsize=figure_size)
#            time_ax = fig_time.add_subplot(1, 1, 1)
#            title = titlebase
#            time_ax.set_title(title, fontsize=titlesize)
#            time_ax.set_xlabel('Time (s)', fontsize=axissize)
#            time_ax.set_ylabel(ylabel, fontsize=axissize)
##            time_ax.grid(b = True, which = 'both')
##           time_ax.tick_params(axis='both', which='major', labelsize=int(axissize*0.8))
#            maxy = 0
#            miny = 1e100
#            colormap = plt.get_cmap('viridis')
##            elidx = 0
#            for algorithm, ydata in sorted([(algorithm, maxfdata[:, indices[algorithm]]) for algorithm in combination], key=lambda tup: -np.nanmean(tup[1])):
#                algoidx = indices[algorithm]
##               colorid = (algoidx - minindex) / (maxindex - minindex)
##                colorid = elidx / (len(combination) - 1)
##                elidx = elidx + 1
##                ydata = maxfdata[:, algoidx]
#                time_ax.plot(time, ydata, linewidth=1, label=algorithm.replace('F', "G'").replace('M',"C'").replace("S","T'"), color=colors[algoidx-1])#, color=colormap(colorid))
##                time_ax.scatter(time, ydata, s=0.2, label=algorithm.replace('F', "G'").replace('M',"C'").replace("S","T'"))#, color=colormap(colorid))
#                maxy = max(maxy, np.nanmax(ydata))
#                miny = min(miny, np.nanmin(ydata))
#            legend = time_ax.legend(fontsize = legendsize, loc='best', ncol=1)
##            for entry in legend.legendHandles:
##                entry.set_sizes(np.array([50]))
#            time_ax.set_xlim(min(time), max(time))
#            if (maxy - miny > 1000):
#                time_ax.set_yscale('log')
#                time_ax.set_ylim(max(1e-1, miny), maxy)
#            else:
#                time_ax.set_ylim(0, maxy)
#            fig_time.tight_layout()
#            fig_time.savefig(title+'_'+'_'.join(combination)+'.pdf')
# 
#    
#    
#    # Impact charts
#    for name, labels in {
#        'G': { exp : idxfor(exp) for exp in ['G', 'F']},
#        'T': { exp : idxfor(exp) for exp in ['T', 'S']},
#        'C': { exp : idxfor(exp) for exp in ['C', 'M']}
#    }.items():
#        fig_impact = plt.figure(figsize=figure_size)
#        impact_ax = fig_impact.add_subplot(1, 1, 1)
#        title = titlebase + ' - alternative ' + name
#        impact_ax.set_title(title, fontsize=titlesize)
#        impact_ax.set_xlabel('Time (s)', fontsize=axissize)
#        impact_ax.set_ylabel('Introduced error', fontsize=axissize)
#        maxy = 0
#        miny = 1e100
##        colormap = plt.get_cmap('viridis')
#        columns = [maxfdata[:, labels[algorithm]] for algorithm in labels]
#        columns = columns[1]-columns[0]
#        ydata = np.nanmean(columns, axis=1)
#        yerr = np.nanstd(columns, axis=1)
#        datamean = np.nanmean(ydata)
#        datastd = np.nanstd(ydata)
#        ydata = [datastd if abs(p-datamean) > 5 * datastd else p for p in ydata]
##        elidx = 0
#        impact_ax.scatter(time, ydata, s=2, color='black')
#        impact_ax.set_ylim(np.nanmin(ydata), np.nanmax(ydata))
#        impact_ax.set_xlim(min(time), max(time))
#        fig_impact.tight_layout()
#        fig_impact.savefig(title+'.pdf')
